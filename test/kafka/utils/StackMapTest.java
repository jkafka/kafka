package kafka.utils;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class StackMapTest {

	private MergerStub merger;
	private StackMap<String, String> map;


	private static class MergerStub implements Merger<String> {
		@Override
		public String merge(String left, String right) {
			return left + right;
		}
	}
	
	@Before
	public void createSut() {
		merger = new MergerStub();
		map = new StackMap<String, String>(merger);
	}

	
	@Test
	public void shouldContainAddedEntry() {
		map.put("one", "1");
		map.put("two", "2");

		assertEquals("1", map.get("one"));
		assertEquals("2", map.get("two"));
		assertTrue(map.containsKey("one"));
		assertTrue(map.containsKey("two"));
		assertFalse(map.containsKey("three"));
	}
	
	@Test
	public void shouldMergeValueSetInTwoBranches() {
		map.put("a", "1");
		
		map.pushBranch();
		map.lbranch();
		map.put("a", "2");
		
		map.rbranch();
		map.put("a", "3");
		
		map.popBranch();
		assertEquals("23", map.get("a"));
	}
	
	@Test
	public void shouldMergeValueSetInLeftBranchWithValueInParentBranch() {
		map.put("a", "1");
		
		map.pushBranch();
		map.lbranch();
		map.put("a", "2");
		
		map.rbranch();
		map.popBranch();
		
		assertEquals("21", map.get("a"));
	}
	
	@Test
	public void shouldMergeValueSetInRightBranchWithValueInParentBranch() {
		map.put("a", "1");
		
		map.pushBranch();
		map.lbranch();
		
		map.rbranch();
		map.put("a", "2");
		map.popBranch();
		
		assertEquals("12", map.get("a"));
	}
}
