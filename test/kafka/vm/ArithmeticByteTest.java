package kafka.vm;

import static org.junit.Assert.assertEquals;
import kafka.ir.instances.Instance;
import kafka.ir.types.Class;

import org.junit.Test;

public class ArithmeticByteTest extends VmTestBase {

	static final Class THIS = ir(ArithmeticByteTest.class);

	private Instance invoke(String name, Instance... arguments) {
		return super.invoke(THIS, name, "(BB)B", arguments);
	}

	@Test
	public void testArithmetic() {
		assertEquals(ir((byte)-10), invoke("addValues", B0, B_10));
		assertEquals(ir((byte)110), invoke("subtractValues", B100, B_10));
		assertEquals(ir((byte)-20), invoke("multiplyValues", B2, B_10));
		assertEquals(ir((byte)-10), invoke("divideValues", B100, B_10));
		assertEquals(ir((byte)10), invoke("negateValues", B_10, B_10));
	}

	@Test
	public void testLogic() {
		assertEquals(B2, invoke("shiftValues", B3, B1));
		assertEquals(ir((byte)(3 % 2)), invoke("reminderValues", B3, B2));
		assertEquals(ir((byte)(3 & 2)), invoke("andValues", B3, B2));
		assertEquals(ir((byte)(1 | 2)), invoke("orValues", B1, B2));
		assertEquals(ir((byte)(3 ^ 2)), invoke("xorValues", B3, B2));
		assertEquals(ir((byte)~100), invoke("complementValues", B100, B0));		
	}

	
	public static byte addValues(byte a, byte b) {
		return (byte)(a + b);
	}

	public static byte subtractValues(byte a, byte b) {
		return (byte)(a - b);
	}

	public static byte multiplyValues(byte a, byte b) {
		return (byte)(a * b);
	}

	public static byte divideValues(byte a, byte b) {
		return (byte)(a / b);
	}	

	public static byte negateValues(byte a, byte b) {
		return (byte)-a;
	}	
	
	public static byte shiftValues(byte a, byte b) {
		return (byte)((a >> b) << b);
	}
	
	public static byte reminderValues(byte a, byte b) {
		return (byte)(a % b);
	}	
	
	public static byte andValues(byte a, byte b) {
		return (byte)(a & b);
	}
	
	public static byte orValues(byte a, byte b) {
		return (byte)(a | b);
	}
	
	public static byte xorValues(byte a, byte b) {
		return (byte)(a ^ b);
	}
	
	public static byte complementValues(byte a, byte b) {
		return (byte)~a;
	}
}
