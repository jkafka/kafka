package kafka.vm;

import static org.junit.Assert.assertEquals;
import kafka.ir.instances.Instance;
import kafka.ir.types.Class;

import org.junit.Test;

public class ConditionalsTest extends VmTestBase {

	static final Class THIS = ir(ConditionalsTest.class);

	private Instance invoke(String name, Instance... arguments) {
		return super.invoke(THIS, name, "(II)I", arguments);
	}

	@Test
	public void testIfStatement() {
		assertEquals(ir(10), invoke("ifStatement", I_10, I_10));
		assertEquals(ir(20), invoke("ifStatement", I1, I1));
		assertEquals(ir(30), invoke("ifStatement", I1, I2));
		assertEquals(ir(40), invoke("ifStatement", I3, I2));		
	}

	public static int ifStatement(int a, int b) {
		if (a == b) {
			int c = 0;
			if (a < 0) 
				c = 10;
			else
				c = 20;
			return c;
		} else if (a * 2 == b) {
			return 30;
		}
		return 40;
	}
	
	@Test
	public void testIfStatementWithReferences() {
		final Instance OBJECT = ir(Object.class).instantiate(BUILDER);
		final Instance NULL = BUILDER.buildNull();
		String objname = vmname(Object.class);
		assertEquals(ir(10), super.invoke(THIS, "ifStatementwithReferences", "(" + objname + ")I", NULL));
		assertEquals(ir(20), super.invoke(THIS, "ifStatementwithReferences", "(" + objname + ")I", OBJECT));
	}

	@SuppressWarnings("unused")
	public static int ifStatementwithReferences(Object o) {
		if (o == null)
			return 10;
		if (o != null)
			return 20;
		return 30;
	}
	
	
	
	@Test
	public void testSwitchStatement() {
		assertEquals(ir(10), invoke("switchStatement", I0, I0));
		assertEquals(ir(20), invoke("switchStatement", I1, I0));
		assertEquals(ir(30), invoke("switchStatement", ir(100), I0));
		assertEquals(ir(40), invoke("switchStatement", ir(-1000), I0));		
		assertEquals(ir(50), invoke("switchStatement", ir(-1001), I0));		
	}

	public static int switchStatement(int a, int dummy) {
		switch(a) {
		case 0:
			return 10;
		case 1:
			return 20;
		case 100:
			return 30;
		case -1000:
			return 40;
		default:
			return 50;
		}
	}

	@Test
	public void testSwitchStatement2() {
		assertEquals(ir(10), invoke("switchStatement2", I0, I0));
		assertEquals(ir(20), invoke("switchStatement2", I1, I0));
		assertEquals(ir(30), invoke("switchStatement2", I2, I0));
		assertEquals(ir(40), invoke("switchStatement2", ir(-1000), I0));		
	}

	public static int switchStatement2(int a, int dummy) {
		switch(a) {
		case 0:
			return 10;
		case 1:
			return 20;
		case 2:
			return 30;
		default:
			return 40;
		}
	}
	
	@Test
	public void testForLoop() {
		assertEquals(I0, invoke("forLoop", I0, I0));
		assertEquals(I0, invoke("forLoop", I0, I1));
		assertEquals(I1, invoke("forLoop", I0, I2));
		assertEquals(ir(1 + 2 + 3 + 4), invoke("forLoop", I0, ir(5)));
	}

	public static int forLoop(int a, int b) {
		int sum = 0;
		for (int i = a; i < b; i++)
			sum += i;
		return sum;
	}

}
