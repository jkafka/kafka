package kafka.vm;

import static org.junit.Assert.assertEquals;
import kafka.ir.instances.Instance;
import kafka.ir.types.Class;

import org.junit.Test;

public class ThrowExcpetionTest extends VmTestBase {

	static final Class THIS = ir(ThrowExcpetionTest.class);
	
	private Instance invoke(Instance integer) {
		return super.invoke(THIS, "throwException", "(I)I", integer);
	}

	@Test
	public void test0() {
		assertEquals(I0, invoke(I0));
		assertEquals(I1, invoke(I1));
		assertEquals(I2, invoke(I2));		
		assertEquals(I2, invoke(I3));		
		assertEquals(I_10, invoke(I_10));		
	}

	public static int throwException(int i) {
		try {
			if (i == 0)
				throw new ArithmeticException();
			else if (i == 1)
				throw new ArrayIndexOutOfBoundsException();
			else if (i == 2)
				throw new ArrayStoreException();			
			else if (i == 3)
				throw new ClassCastException();
		} catch (ArithmeticException ex) {
			return 0;
		} catch (ArrayIndexOutOfBoundsException ex) {
			return 1;
		} catch (RuntimeException ex) {
			return 2;
		}
		return -10;
	}
}
