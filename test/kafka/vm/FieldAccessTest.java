package kafka.vm;

import static org.junit.Assert.*;
import kafka.ir.instances.Instance;
import kafka.ir.types.Class;

import org.junit.Test;

public class FieldAccessTest extends VmTestBase {

	public static class MyClass {
		public static int staticInt;
		public int instanceInt;
	}
	static final Class THIS = ir(FieldAccessTest.class);
	
	@Test
	public void testWriteReadStaticField() {
		Instance result = invoke(THIS, "writeReadStaticField", "(I)I", I3);
		assertEquals(I3, result);
	}

	public static int writeReadStaticField(int value) {
		MyClass.staticInt = value;
		return MyClass.staticInt;
	}
	
	
	@Test
	public void testWriteReadInstanceField() {
		Instance result = invoke(THIS, "writeReadInstanceField", "(I)I", I3);
		assertEquals(I3, result);
	}

	public static int writeReadInstanceField(int value) {
		MyClass instance = new MyClass();
		instance.instanceInt = value;
		return instance.instanceInt;
	}
}
