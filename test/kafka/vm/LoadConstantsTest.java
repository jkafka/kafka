package kafka.vm;

import static org.junit.Assert.assertEquals;
import kafka.ir.instances.Instance;
import kafka.ir.instances.Primitives;
import kafka.ir.types.Class;

import org.junit.Ignore;
import org.junit.Test;

/**
 * Tests handling of the large constants in the VM.
 */
public class LoadConstantsTest extends VmTestBase {

	static final Class THIS = ir(LoadConstantsTest.class);

	@Test
	public void testLoadDouble() {
		Primitives.Double result = (Primitives.Double)invoke(THIS, "loadDouble", "()D");
		assertEquals(ir(1234.567d), result);
	}
	
	static double loadDouble() {
		return 1234.567d;
	}
	
	@Test
	public void testLargeFloat() {
		Primitives.Float result = (Primitives.Float)invoke(THIS, "loadFloat", "()F");
		assertEquals(ir(1234.567f), result);
	}
	
	static float loadFloat() {
		return 1234.567f;
	}
	
	@Test
	public void testLoadInteger() {
		Instance result = invoke(THIS, "loadInteger", "()I");
		assertEquals(ir(1 << 31), result);
	}
	
	static int loadInteger() {
		return 1 << 31;
	}
	
	@Test
	public void testLoadLong() {
		Primitives.Long result = (Primitives.Long)invoke(THIS, "loadLong", "()J");
		assertEquals(ir(1L << 63), result);
	}
	
	static long loadLong() {
		return 1L << 63;
	}

	@Test
	public void testLoadNull() {
		Instance result = invoke(THIS, "loadNull", "()Ljava/lang/Object;");
		assertEquals(BUILDER.buildNull(), result);
	}
	
	static Object loadNull() {
		return null;
	}
	
	@Test
	@Ignore("Strings are not handled in the VM yet.")
	public void testLoadString() {
		Primitives.Double result = (Primitives.Double)invoke(THIS, "loadString", "()" + vmname(String.class));
		assertEquals(ir(10), result);
	}
	
	static String loadString() {
		return "my string";
	}

}
