package kafka.vm;

import static org.junit.Assert.assertEquals;
import kafka.ir.instances.Instance;
import kafka.ir.types.Class;

import org.junit.Test;

public class ArithmeticFloatTest extends VmTestBase {

	static final Class THIS = ir(ArithmeticFloatTest.class);

	private Instance invoke(String name, Instance... arguments) {
		return super.invoke(THIS, name, "(FF)F", arguments);
	}

	@Test
	public void testArithmetic() {
		assertEquals(ir((float)-10), invoke("addValues", F0, F_10));
		assertEquals(ir((float)1010), invoke("subtractValues", F1000, F_10));
		assertEquals(ir((float)-20), invoke("multiplyValues", F2, F_10));
		assertEquals(ir((float)-100), invoke("divideValues", F1000, F_10));
		assertEquals(ir((float)10), invoke("negateValues", F_10, F_10));
	}

	public static float addValues(float a, float b) {
		return (float)(a + b);
	}

	public static float subtractValues(float a, float b) {
		return (float)(a - b);
	}

	public static float multiplyValues(float a, float b) {
		return (float)(a * b);
	}

	public static float divideValues(float a, float b) {
		return (float)(a / b);
	}	
	
	public static float reminderValues(float a, float b) {
		return (float)(a % b);
	}	
	
	public static float negateValues(float a, float b) {
		return -a;
	}	
	
}
