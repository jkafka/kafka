package kafka.vm;

import static org.junit.Assert.assertEquals;
import kafka.ir.instances.Instance;
import kafka.ir.types.Class;

import org.junit.Test;

public class StaticConstructor extends VmTestBase {

	static final Class THIS = ir(StaticConstructor.class);
	
	private Instance invoke() {
		String descr = "()I";
		return super.invoke(THIS, "readStaticField", descr);
	}
	@Test
	public void shouldExecuteStaticConstructorUponClassLoading() {
		assertEquals(I1, invoke());
	}

	
	static class MyClass {
		static int FIELD;
		static {
			FIELD = 1;
		}
	}
	
	public static int readStaticField() {
		return MyClass.FIELD;
	}
}
