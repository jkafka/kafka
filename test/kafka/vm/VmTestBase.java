package kafka.vm;

import java.util.Map;


import kafka.ir.Builder;

import kafka.ir.instances.Instance;
import kafka.ir.instances.Primitives;
import kafka.ir.types.Class;
import kafka.ir.types.Method;
import kafka.ir.types.Type;
import kafka.natives.Natives;

public class VmTestBase {

	// Share the builder between all tests to make the tests run faster. We thus
	// assume that there is no state in the builder (except caching).
	protected static final Builder BUILDER = new Builder();
	
	private static final Map<Class, Object> NATIVES = Natives.init(BUILDER);	
	protected static final Primitives.Int I_10 = ir(-10);
	protected static final Primitives.Int I0 = ir(0);
	protected static final Primitives.Int I1 = ir(1);
	protected static final Primitives.Int I2 = ir(2);
	protected static final Primitives.Int I3 = ir(3);
	protected static final Primitives.Int I1000 = ir(1000);

	protected static final Primitives.Long L_10 = ir(-10L);
	protected static final Primitives.Long L0 = ir(0L);
	protected static final Primitives.Long L1 = ir(1L);
	protected static final Primitives.Long L2 = ir(2L);
	protected static final Primitives.Long L3 = ir(3L);
	protected static final Primitives.Long L1000 = ir(1000L);

	protected static final Primitives.Short S_10 = ir((short)-10);
	protected static final Primitives.Short S0 = ir((short)0);
	protected static final Primitives.Short S1 = ir((short)1);
	protected static final Primitives.Short S2 = ir((short)2);
	protected static final Primitives.Short S3 = ir((short)3);
	protected static final Primitives.Short S1000 = ir((short)1000);
	
	protected static final Primitives.Byte B_10 = ir((byte)-10);
	protected static final Primitives.Byte B0 = ir((byte)0);
	protected static final Primitives.Byte B1 = ir((byte)1);
	protected static final Primitives.Byte B2 = ir((byte)2);
	protected static final Primitives.Byte B3 = ir((byte)3);
	protected static final Primitives.Byte B100 = ir((byte)100);	

	protected static final Primitives.Double D_10 = ir((double)-10);
	protected static final Primitives.Double D0 = ir((double)0);
	protected static final Primitives.Double D1 = ir((double)1);
	protected static final Primitives.Double D2 = ir((double)2);
	protected static final Primitives.Double D3 = ir((double)3);
	protected static final Primitives.Double D1000 = ir((double)1000);	

	protected static final Primitives.Float F_10 = ir((float)-10);
	protected static final Primitives.Float F0 = ir((float)0);
	protected static final Primitives.Float F1 = ir((float)1);
	protected static final Primitives.Float F2 = ir((float)2);
	protected static final Primitives.Float F3 = ir((float)3);
	protected static final Primitives.Float F1000 = ir((float)1000);
	
	protected static final Primitives.Char C_10 = ir((char)-10);
	protected static final Primitives.Char C0 = ir((char)0);
	protected static final Primitives.Char C1 = ir((char)1);
	protected static final Primitives.Char C2 = ir((char)2);
	protected static final Primitives.Char C3 = ir((char)3);
	protected static final Primitives.Char C100 = ir((char)100);
	
	protected static Instance invoke(Method method, Instance... args) {
		BUILDER.runStaticInitializers(NATIVES);
		return new Vm(NATIVES, BUILDER).invoke(method, args);		
	}

	protected Instance invoke(Class obj, String name, String descriptor, Instance... arguments) {
		Method method = obj.method(BUILDER.buildMethodReference(name, descriptor));
		return invoke(method, arguments);
	}

	protected static Primitives.Double ir(double i) {
		return BUILDER.buildDouble(i);
	}

	protected static Primitives.Float ir(float i) {
		return BUILDER.buildFloat(i);
	}

	protected static Primitives.Int ir(int i) {
		return BUILDER.buildInt(i);
	}
	
	protected static Primitives.Short ir(short i) {
		return BUILDER.buildShort(i);
	}	

	protected static Primitives.Byte ir(byte i) {
		return BUILDER.buildByte(i);
	}	
	
	protected static Primitives.Char ir(char i) {
		return BUILDER.buildChar(i);
	}
	
	protected static Primitives.Long ir(long i) {
		return BUILDER.buildLong(i);
	}
	
	protected static Class ir(java.lang.Class<?> cls) {
		return BUILDER.buildClass(vmname(cls));
	}
	
	protected static Type typeForName(String name) {
		return BUILDER.buildType(name);
	}
	
	protected static String vmname(java.lang.Class<?> cls) {
		return "L" + cls.getName().replace('.', '/') + ";";
	}


}