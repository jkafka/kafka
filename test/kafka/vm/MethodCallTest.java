package kafka.vm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import java.util.concurrent.Callable;

import kafka.ir.instances.Instance;
import kafka.ir.types.Class;

import org.junit.Test;

public class MethodCallTest extends VmTestBase {

	static final Class THIS = ir(MethodCallTest.class);

	static class Dummy implements Callable<Object> {
		@Override
		public Object call() {
			return this;
		}

		public final Object finalMethod() {
			return privateMethod();
		}

		private Object privateMethod() {
			return this;
		}
	}
	
	@Test
	public void testInvokeStaticMethod() {
		assertSame(I0, invoke(THIS, "invokeStaticMethod", "()I"));
	}

	@Test
	public void testInvokeConstructor() {
		Instance result = invoke(THIS, "invokeConstructor", "()" + vmname(Dummy.class));
		assertEquals(ir(Dummy.class), result.type());
	}
	
	@Test
	public void testInvokeVirtualMethod() {
		Instance result = invoke(THIS, "invokeVirtualMethod", "()" + vmname(Object.class));
		assertEquals(ir(Dummy.class), result.type());
	}

	@Test
	public void testInvokeNonVirtualMethod() {
		Instance result = invoke(THIS, "invokeNonVirtualMethod", "()" + vmname(Object.class));
		assertEquals(ir(Dummy.class), result.type());
	}

	@Test
	public void testInvokeInterfaceMethod() {
		Instance result = invoke(THIS, "invokeInterfaceMethod", "()" + vmname(Object.class));
		assertEquals(ir(Dummy.class), result.type());
	}
	
	static int func0() {
		return 0;
	}
	
	static int invokeStaticMethod() {
		return func0();
	}

	static Dummy invokeConstructor() {
		return new Dummy();
	}
	
	static Object invokeVirtualMethod() {
		return new Dummy().call();
	}
	
	static Object invokeNonVirtualMethod() {
		return new Dummy().finalMethod();
	}
	
	static Object call(Callable<Object> c) throws Exception {
		return c.call();
	}
	
	static Object invokeInterfaceMethod() throws Exception {
		return call(new Dummy());
	}
}
