package kafka.vm;

import static org.junit.Assert.assertEquals;
import kafka.ir.instances.Instance;
import kafka.ir.types.Class;

import org.junit.Test;

public class ComparisonFloatTest extends VmTestBase {

	static final Class THIS = ir(ComparisonFloatTest.class);

	private Instance invoke(String name, Instance... arguments) {
		return super.invoke(THIS, name, "(FF)Z", arguments);
	}

	@Test
	public void testLessThan() {
		assertEquals(I1, invoke("lessThan", F0, F1));
		assertEquals(I0, invoke("lessThan", F1, F0));
		assertEquals(I0, invoke("lessThan", F0, F0));
	}

	@Test
	public void testGreaterThan() {
		assertEquals(I0, invoke("greaterThan", F0, F1));
		assertEquals(I1, invoke("greaterThan", F1, F0));
		assertEquals(I0, invoke("greaterThan", F0, F0));
	}

	@Test
	public void testEqualTo() {
		assertEquals(I0, invoke("equalTo", F0, F1));
		assertEquals(I0, invoke("equalTo", F1, F0));
		assertEquals(I1, invoke("equalTo", F0, F0));
	}

	@Test
	public void testNotEqualTo() {	
		assertEquals(I1, invoke("notEqualTo", F0, F1));
		assertEquals(I1, invoke("notEqualTo", F1, F0));
		assertEquals(I0, invoke("notEqualTo", F0, F0));
	}

	@Test
	public void testLessThanOrEqualTo() {	
		assertEquals(I1, invoke("lessThanOrEqualTo", F0, F1));
		assertEquals(I0, invoke("lessThanOrEqualTo", F1, F0));
		assertEquals(I1, invoke("lessThanOrEqualTo", F0, F0));
	}

	@Test
	public void testGreaterThanOrEqualTo() {	
		assertEquals(I0, invoke("greaterThanOrEqualTo", F0, F1));
		assertEquals(I1, invoke("greaterThanOrEqualTo", F1, F0));
		assertEquals(I1, invoke("greaterThanOrEqualTo", F0, F0));
	}
	
	public static boolean equalTo(float a, float b) {
		return a == b;
	}

	public static boolean greaterThan(float a, float b) {
		return a > b;
	}

	public static boolean lessThan(float a, float b) {
		return a < b;
	}
	
	public static boolean notEqualTo(float a, float b) {
		return a != b;
	}

	public static boolean lessThanOrEqualTo(float a, float b) {
		return a <= b;
	}

	public static boolean greaterThanOrEqualTo(float a, float b) {
		return a >= b;
	}	
	
}
