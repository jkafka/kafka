package kafka.vm;

import static org.junit.Assert.assertEquals;
import kafka.ir.instances.Instance;
import kafka.ir.types.Class;

import org.junit.Test;

public class PrimitivesLoadStoreTest extends VmTestBase {

	static final Class THIS = ir(PrimitivesLoadStoreTest.class);

	private Instance invoke(String name, Instance... arguments) {
		return super.invoke(THIS, name, "(IJFD)D", arguments);
	}

	@Test
	public void testArithmetic() {
		assertEquals(ir((double)4), invoke("loadStorePrimitives", I1, L1, F1, D1));
	}

	
	public static double loadStorePrimitives(int i, long l, float f, double d) {
		int ii = i;
		long ll = l;
		float ff = f;
		double dd = d;
		return ii + ll + ff + dd;
	}
}
