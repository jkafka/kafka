package kafka.vm;

import static org.junit.Assert.assertEquals;
import kafka.ir.instances.Instance;
import kafka.ir.types.Class;

import org.junit.Test;

public class ArithmeticDoubleTest extends VmTestBase {

	static final Class THIS = ir(ArithmeticDoubleTest.class);

	private Instance invoke(String name, Instance... arguments) {
		return super.invoke(THIS, name, "(DD)D", arguments);
	}

	@Test
	public void testArithmetic() {
		assertEquals(ir((double)-10), invoke("addValues", D0, D_10));
		assertEquals(ir((double)1010), invoke("subtractValues", D1000, D_10));
		assertEquals(ir((double)-20), invoke("multiplyValues", D2, D_10));
		assertEquals(ir((double)-100), invoke("divideValues", D1000, D_10));
		assertEquals(ir((double)10), invoke("negateValues", D_10, D_10));
	}

	public static double addValues(double a, double b) {
		return (double)(a + b);
	}

	public static double subtractValues(double a, double b) {
		return (double)(a - b);
	}

	public static double multiplyValues(double a, double b) {
		return (double)(a * b);
	}

	public static double divideValues(double a, double b) {
		return (double)(a / b);
	}	
	
	public static double reminderValues(double a, double b) {
		return (double)(a % b);
	}	
	
	public static double negateValues(double a, double b) {
		return -a;
	}	

}
