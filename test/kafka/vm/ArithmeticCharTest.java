package kafka.vm;

import static org.junit.Assert.assertEquals;
import kafka.ir.instances.Instance;
import kafka.ir.types.Class;

import org.junit.Test;

public class ArithmeticCharTest extends VmTestBase {

	static final Class THIS = ir(ArithmeticCharTest.class);

	private Instance invoke(String name, Instance... arguments) {
		return super.invoke(THIS, name, "(CC)C", arguments);
	}

	@Test
	public void testArithmetic() {
		assertEquals(ir((char)-10), invoke("addValues", C0, C_10));
		assertEquals(ir((char)110), invoke("subtractValues", C100, C_10));
		assertEquals(ir((char)-20), invoke("multiplyValues", C2, C_10));
		assertEquals(ir((char)50), invoke("divideValues", C100, C2));
		assertEquals(ir((char)10), invoke("negateValues", C_10, C_10));		
	}

	@Test
	public void testLogic() {
		assertEquals(C100, invoke("shiftValues", C100, C2));
		assertEquals(ir((char)(3 % 2)), invoke("reminderValues", C3, C2));
		assertEquals(ir((char)(3 & 2)), invoke("andValues", C3, C2));
		assertEquals(ir((char)(1 | 2)), invoke("orValues", C1, C2));
		assertEquals(ir((char)(3 ^ 2)), invoke("xorValues", C3, C2));
		assertEquals(ir((char)~100), invoke("complementValues", C100, C0));		
	}

	
	public static char addValues(char a, char b) {
		return (char)(a + b);
	}

	public static char subtractValues(char a, char b) {
		return (char)(a - b);
	}

	public static char multiplyValues(char a, char b) {
		return (char)(a * b);
	}

	public static char divideValues(char a, char b) {
		return (char)(a / b);
	}	
	
	public static char negateValues(char a, char b) {
		return (char)-a;
	}	
	
	public static char shiftValues(char a, char b) {
		return (char)((a >> b) << b);
	}
	
	public static char reminderValues(char a, char b) {
		return (char)(a % b);
	}	
	
	public static char andValues(char a, char b) {
		return (char)(a & b);
	}
	
	public static char orValues(char a, char b) {
		return (char)(a | b);
	}
	
	public static char xorValues(char a, char b) {
		return (char)(a ^ b);
	}
	
	public static char complementValues(char a, char b) {
		return (char)~a;
	}
}
