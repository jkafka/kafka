package kafka.vm;

import static org.junit.Assert.assertEquals;
import kafka.ir.instances.Instance;
import kafka.ir.types.Class;

import org.junit.Test;

public class ComparisonIntTest extends VmTestBase {

	static final Class THIS = ir(ComparisonIntTest.class);

	private Instance invoke(String name, Instance... arguments) {
		return super.invoke(THIS, name, "(II)Z", arguments);
	}

	@Test
	public void testLessThan() {
		assertEquals(I1, invoke("lessThan", I0, I1));
		assertEquals(I0, invoke("lessThan", I1, I0));
		assertEquals(I0, invoke("lessThan", I0, I0));
	}

	@Test
	public void testGreaterThan() {
		assertEquals(I0, invoke("greaterThan", I0, I1));
		assertEquals(I1, invoke("greaterThan", I1, I0));
		assertEquals(I0, invoke("greaterThan", I0, I0));
	}

	@Test
	public void testEqualTo() {
		assertEquals(I0, invoke("equalTo", I0, I1));
		assertEquals(I0, invoke("equalTo", I1, I0));
		assertEquals(I1, invoke("equalTo", I0, I0));
	}

	@Test
	public void testNotEqualTo() {	
		assertEquals(I1, invoke("notEqualTo", I0, I1));
		assertEquals(I1, invoke("notEqualTo", I1, I0));
		assertEquals(I0, invoke("notEqualTo", I0, I0));
	}

	@Test
	public void testLessThanOrEqualTo() {	
		assertEquals(I1, invoke("lessThanOrEqualTo", I0, I1));
		assertEquals(I0, invoke("lessThanOrEqualTo", I1, I0));
		assertEquals(I1, invoke("lessThanOrEqualTo", I0, I0));
	}

	@Test
	public void testGreaterThanOrEqualTo() {	
		assertEquals(I0, invoke("greaterThanOrEqualTo", I0, I1));
		assertEquals(I1, invoke("greaterThanOrEqualTo", I1, I0));
		assertEquals(I1, invoke("greaterThanOrEqualTo", I0, I0));
	}
	
	public static boolean equalTo(int a, int b) {
		return a == b;
	}

	public static boolean greaterThan(int a, int b) {
		return a > b;
	}

	public static boolean lessThan(int a, int b) {
		return a < b;
	}
	
	public static boolean notEqualTo(int a, int b) {
		return a != b;
	}

	public static boolean lessThanOrEqualTo(int a, int b) {
		return a <= b;
	}

	public static boolean greaterThanOrEqualTo(int a, int b) {
		return a >= b;
	}	
	
}
