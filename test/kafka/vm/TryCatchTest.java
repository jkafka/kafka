package kafka.vm;

import static org.junit.Assert.assertEquals;
import kafka.ir.instances.Instance;
import kafka.ir.types.Class;

import org.junit.Test;

public class TryCatchTest extends VmTestBase {

	static final Class THIS = ir(TryCatchTest.class);
	
	private Instance invoke(Instance instance) {
		String descr = "(" + vmname(Object.class) + ")Z";
		return super.invoke(THIS, "checkcast", descr, instance);
	}

	@Test
	public void shouldCastSubclass() {
		Instance instance = ir(MyClass.class).instantiate(BUILDER);
		assertEquals(I1, invoke(instance));
	}

	@Test
	public void shouldRaiseClassCastException() {
		Instance instance = ir(OtherClass.class).instantiate(BUILDER);
		assertEquals(I0, invoke(instance));
	}


	public static final class OtherClass { }
	public static final class MyClass { }
	
	public static boolean checkcast(Object o) {
		try {
			@SuppressWarnings("unused")
			MyClass c = (MyClass)o;
			return true;
		} catch (ClassCastException ex) {
			return false;
		}
	}
}
