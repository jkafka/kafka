package kafka.vm;

import static org.junit.Assert.assertEquals;
import kafka.ir.instances.Instance;
import kafka.ir.types.Class;

import org.junit.Test;

public class ComparisonDoubleTest extends VmTestBase {

	static final Class THIS = ir(ComparisonDoubleTest.class);

	private Instance invoke(String name, Instance... arguments) {
		return super.invoke(THIS, name, "(DD)Z", arguments);
	}

	@Test
	public void testLessThan() {
		assertEquals(I1, invoke("lessThan", D0, D1));
		assertEquals(I0, invoke("lessThan", D1, D0));
		assertEquals(I0, invoke("lessThan", D0, D0));
	}

	@Test
	public void testGreaterThan() {
		assertEquals(I0, invoke("greaterThan", D0, D1));
		assertEquals(I1, invoke("greaterThan", D1, D0));
		assertEquals(I0, invoke("greaterThan", D0, D0));
	}

	@Test
	public void testEqualTo() {
		assertEquals(I0, invoke("equalTo", D0, D1));
		assertEquals(I0, invoke("equalTo", D1, D0));
		assertEquals(I1, invoke("equalTo", D0, D0));
	}

	@Test
	public void testNotEqualTo() {	
		assertEquals(I1, invoke("notEqualTo", D0, D1));
		assertEquals(I1, invoke("notEqualTo", D1, D0));
		assertEquals(I0, invoke("notEqualTo", D0, D0));
	}

	@Test
	public void testLessThanOrEqualTo() {	
		assertEquals(I1, invoke("lessThanOrEqualTo", D0, D1));
		assertEquals(I0, invoke("lessThanOrEqualTo", D1, D0));
		assertEquals(I1, invoke("lessThanOrEqualTo", D0, D0));
	}

	@Test
	public void testGreaterThanOrEqualTo() {	
		assertEquals(I0, invoke("greaterThanOrEqualTo", D0, D1));
		assertEquals(I1, invoke("greaterThanOrEqualTo", D1, D0));
		assertEquals(I1, invoke("greaterThanOrEqualTo", D0, D0));
	}
	
	public static boolean equalTo(double a, double b) {
		return a == b;
	}

	public static boolean greaterThan(double a, double b) {
		return a > b;
	}

	public static boolean lessThan(double a, double b) {
		return a < b;
	}
	
	public static boolean notEqualTo(double a, double b) {
		return a != b;
	}

	public static boolean lessThanOrEqualTo(double a, double b) {
		return a <= b;
	}

	public static boolean greaterThanOrEqualTo(double a, double b) {
		return a >= b;
	}	
	
}
