package kafka.vm;

import static org.junit.Assert.*;
import kafka.ir.types.Class;

import org.junit.Test;

public class InstanceOfTest extends VmTestBase {

	public static interface Interface { }
	public static class A implements Interface { }
	public static class B extends A { }
	public static class C { }
	
	static final Class THIS = ir(InstanceOfTest.class);
	
	@Test
	public void testInstanceOfInterface() {
		assertEquals(I1, invoke(THIS, "instanceOfInterface0", "()Z"));
		assertEquals(I0, invoke(THIS, "instanceOfInterface1", "()Z"));
	}

	public static boolean instanceOfInterface0() {
		return new A() instanceof Interface;
	}
	
	public static boolean instanceOfInterface1() {
		return new C() instanceof Interface;
	}
	

	@Test
	public void testInstanceOfClass() {
		assertEquals(I1, invoke(THIS, "instanceOfClass0", "()Z"));
		assertEquals(I1, invoke(THIS, "instanceOfClass1", "()Z"));
		assertEquals(I0, invoke(THIS, "instanceOfClass2", "()Z"));
	}

	public static boolean instanceOfClass0() {
		return new A() instanceof A;
	}

	public static boolean instanceOfClass1() {
		return new B() instanceof A;
	}

	public static boolean instanceOfClass2() {
		return ((Object)new C()) instanceof A;
	}

}
