package kafka.vm;

import static org.junit.Assert.assertEquals;
import kafka.ir.instances.Instance;
import kafka.ir.types.Class;

import org.junit.Test;

public class ArithmeticShortTest extends VmTestBase {

	static final Class THIS = ir(ArithmeticShortTest.class);

	private Instance invoke(String name, Instance... arguments) {
		return super.invoke(THIS, name, "(SS)S", arguments);
	}

	@Test
	public void testArithmetic() {
		assertEquals(ir((short)-10), invoke("addValues", S0, S_10));
		assertEquals(ir((short)1010), invoke("subtractValues", S1000, S_10));
		assertEquals(ir((short)-20), invoke("multiplyValues", S2, S_10));
		assertEquals(ir((short)-100), invoke("divideValues", S1000, S_10));
		assertEquals(ir((short)10), invoke("negateValues", S_10, S_10));		
	}

	@Test
	public void testLogic() {
		assertEquals(S2, invoke("shiftValues", S3, S1));
		assertEquals(ir((short)(3 % 2)), invoke("reminderValues", S3, S2));
		assertEquals(ir((short)(3 & 2)), invoke("andValues", S3, S2));
		assertEquals(ir((short)(1 | 2)), invoke("orValues", S1, S2));
		assertEquals(ir((short)(3 ^ 2)), invoke("xorValues", S3, S2));
		assertEquals(ir((short)~1000), invoke("complementValues", S1000, S0));		
	}

	
	public static short addValues(short a, short b) {
		return (short)(a + b);
	}

	public static short subtractValues(short a, short b) {
		return (short)(a - b);
	}

	public static short multiplyValues(short a, short b) {
		return (short)(a * b);
	}

	public static short divideValues(short a, short b) {
		return (short)(a / b);
	}	
	
	public static short negateValues(short a, short b) {
		return (short)-a;
	}	
	
	public static short shiftValues(short a, short b) {
		return (short)((a >> b) << b);
	}
	
	public static short reminderValues(short a, short b) {
		return (short)(a % b);
	}	
	
	public static short andValues(short a, short b) {
		return (short)(a & b);
	}
	
	public static short orValues(short a, short b) {
		return (short)(a | b);
	}
	
	public static short xorValues(short a, short b) {
		return (short)(a ^ b);
	}
	
	public static short complementValues(short a, short b) {
		return (short)~a;
	}
}
