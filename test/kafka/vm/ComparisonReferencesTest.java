package kafka.vm;

import static org.junit.Assert.assertEquals;
import kafka.ir.instances.Instance;
import kafka.ir.types.Class;

import org.junit.Test;

public class ComparisonReferencesTest extends VmTestBase {

	static final Class THIS = ir(ComparisonReferencesTest.class);
	static final Instance OBJECT0 = ir(Object.class).instantiate(BUILDER);
	static final Instance OBJECT1 = ir(Object.class).instantiate(BUILDER);

	private Instance invoke(String name, Instance... arguments) {
		String objname = vmname(Object.class);
		return super.invoke(THIS, name, "(" + objname + objname + ")Z", arguments);
	}

	@Test
	public void testEqualTo() {
		assertEquals(I1, invoke("equalTo", OBJECT0, OBJECT0));
		assertEquals(I0, invoke("equalTo", OBJECT0, OBJECT1));
	}

	@Test
	public void testNotEqualTo() {	
		assertEquals(I0, invoke("notEqualTo", OBJECT0, OBJECT0));
		assertEquals(I1, invoke("notEqualTo", OBJECT0, OBJECT1));
	}

	public static boolean equalTo(Object a, Object b) {
		return a == b;
	}

	public static boolean notEqualTo(Object a, Object b) {
		return a != b;
	}
	
}
