package kafka.vm;

import static org.junit.Assert.assertEquals;
import kafka.ir.instances.Instance;
import kafka.ir.types.Class;

import org.junit.Test;

public class CheckcastTest extends VmTestBase {

	static final Class THIS = ir(CheckcastTest.class);

	@Test
	public void shouldCastSubclass() {
		Instance instance = ir(MyClass.class).instantiate(BUILDER);
		String descr = "(" + vmname(Object.class) + ")" + vmname(MyClass.class);
		assertEquals(instance, invoke(THIS, "checkcast", descr, instance));
	}

	@Test
	public void shouldRaiseClassCastException() {
		Instance instance = ir(OtherClass.class).instantiate(BUILDER);
		String descr = "(" + vmname(Object.class) + ")" + vmname(MyClass.class);
		ExceptionOccured result = (ExceptionOccured) invoke(THIS, "checkcast", descr, instance);
		assertEquals(ir(ClassCastException.class), result.exception().type());
	}


	public static final class OtherClass { }
	public static final class MyClass { }
	public static MyClass checkcast(Object o) {
		return (MyClass)o;
	}
}
