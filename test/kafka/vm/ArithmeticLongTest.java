package kafka.vm;

import static org.junit.Assert.assertEquals;
import kafka.ir.instances.Instance;
import kafka.ir.types.Class;

import org.junit.Test;

public class ArithmeticLongTest extends VmTestBase {

	static final Class THIS = ir(ArithmeticLongTest.class);

	private Instance invoke(String name, Instance... arguments) {
		return super.invoke(THIS, name, "(JJ)J", arguments);
	}

	@Test
	public void testArithmetic() {
		assertEquals(ir(-10L), invoke("addValues", L0, L_10));
		assertEquals(ir(1010L), invoke("subtractValues", L1000, L_10));
		assertEquals(ir(-20L), invoke("multiplyValues", L2, L_10));
		assertEquals(ir(-100L), invoke("divideValues", L1000, L_10));
		assertEquals(ir(10L), invoke("negateValues", L_10, L_10));		
	}

	@Test
	public void testLogic() {
		assertEquals(L2, invoke("shiftValues", L3, L1));
		assertEquals(ir((long)(3 % 2)), invoke("reminderValues", L3, L2));
		assertEquals(ir((long)(3 & 2)), invoke("andValues", L3, L2));
		assertEquals(ir((long)(1 | 2)), invoke("orValues", L1, L2));
		assertEquals(ir((long)(3 ^ 2)), invoke("xorValues", L3, L2));
		assertEquals(ir((long)~1000), invoke("complementValues", L1000, L0));		
	}

	
	public static long addValues(long a, long b) {
		return a + b;
	}

	public static long subtractValues(long a, long b) {
		return a - b;
	}

	public static long multiplyValues(long a, long b) {
		return a * b;
	}

	public static long divideValues(long a, long b) {
		return a / b;
	}	
	
	public static long negateValues(long a, long b) {
		return -a;
	}	
	
	public static long shiftValues(long a, long b) {
		return (a >> b) << b;
	}
	
	public static long reminderValues(long a, long b) {
		return a % b;
	}	
	
	public static long andValues(long a, long b) {
		return a & b;
	}
	
	public static long orValues(long a, long b) {
		return a | b;
	}
	
	public static long xorValues(long a, long b) {
		return a ^ b;
	}
	
	public static long complementValues(long a, long b) {
		return ~a;
	}
}
