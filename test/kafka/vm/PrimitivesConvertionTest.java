package kafka.vm;

import static org.junit.Assert.assertEquals;
import kafka.ir.instances.Instance;
import kafka.ir.types.Class;

import org.junit.Test;

public class PrimitivesConvertionTest extends VmTestBase {

	static final Class THIS = ir(PrimitivesConvertionTest.class);

	private Instance invoke(String types, Instance... arguments) {
		String from = String.valueOf(types.charAt(0));
		String to = String.valueOf(types.charAt(1));
		String name = from.toLowerCase() + "2" + to.toLowerCase();
		return super.invoke(THIS, name, "(" + from + ")" + to, arguments);
	}

	@Test
	public void testIntConvertions() {
		assertEquals(ir((float)1), invoke("IF", I1));
		assertEquals(ir((double)1), invoke("ID", I1));
		assertEquals(ir((long)1), invoke("IJ", I1));
	}

	@Test
	public void testLongConvertions() {
		assertEquals(ir((float)1), invoke("JF", L1));
		assertEquals(ir((double)1), invoke("JD", L1));
		assertEquals(ir((int)1), invoke("JI", L1));
	}

	@Test
	public void testFloatConvertions() {
		assertEquals(ir((long)1), invoke("FJ", F1));
		assertEquals(ir((double)1), invoke("FD", F1));
		assertEquals(ir((int)1), invoke("FI", F1));
	}
	
	@Test
	public void testDoubleConvertions() {
		assertEquals(ir((long)1), invoke("DJ", D1));
		assertEquals(ir((float)1), invoke("DF", D1));
		assertEquals(ir((int)1), invoke("DI", D1));
	}
	
	public static float i2f(int a) {
		return (float)a;
	}

	public static double i2d(int a) {
		return (double)a;
	}

	public static long i2j(int a) {
		return (long)a;
	}
	
	
	public static float j2f(long a) {
		return (float)a;
	}

	public static double j2d(long a) {
		return (double)a;
	}

	public static int j2i(long a) {
		return (int)a;
	}

	
	public static float d2f(double a) {
		return (float)a;
	}

	public static long d2j(double a) {
		return (long)a;
	}

	public static int d2i(double a) {
		return (int)a;
	}


	public static double f2d(float a) {
		return (float)a;
	}

	public static long f2j(float a) {
		return (long)a;
	}

	public static int f2i(float a) {
		return (int)a;
	}

}
