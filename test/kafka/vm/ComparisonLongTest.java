package kafka.vm;

import static org.junit.Assert.assertEquals;
import kafka.ir.instances.Instance;
import kafka.ir.types.Class;

import org.junit.Test;

public class ComparisonLongTest extends VmTestBase {

	static final Class THIS = ir(ComparisonLongTest.class);

	private Instance invoke(String name, Instance... arguments) {
		return super.invoke(THIS, name, "(JJ)Z", arguments);
	}

	@Test
	public void testLessThan() {
		assertEquals(I1, invoke("lessThan", L0, L1));
		assertEquals(I0, invoke("lessThan", L1, L0));
		assertEquals(I0, invoke("lessThan", L0, L0));
	}

	@Test
	public void testGreaterThan() {
		assertEquals(I0, invoke("greaterThan", L0, L1));
		assertEquals(I1, invoke("greaterThan", L1, L0));
		assertEquals(I0, invoke("greaterThan", L0, L0));
	}

	@Test
	public void testEqualTo() {
		assertEquals(I0, invoke("equalTo", L0, L1));
		assertEquals(I0, invoke("equalTo", L1, L0));
		assertEquals(I1, invoke("equalTo", L0, L0));
	}

	@Test
	public void testNotEqualTo() {	
		assertEquals(I1, invoke("notEqualTo", L0, L1));
		assertEquals(I1, invoke("notEqualTo", L1, L0));
		assertEquals(I0, invoke("notEqualTo", L0, L0));
	}

	@Test
	public void testLessThanOrEqualTo() {	
		assertEquals(I1, invoke("lessThanOrEqualTo", L0, L1));
		assertEquals(I0, invoke("lessThanOrEqualTo", L1, L0));
		assertEquals(I1, invoke("lessThanOrEqualTo", L0, L0));
	}

	@Test
	public void testGreaterThanOrEqualTo() {	
		assertEquals(I0, invoke("greaterThanOrEqualTo", L0, L1));
		assertEquals(I1, invoke("greaterThanOrEqualTo", L1, L0));
		assertEquals(I1, invoke("greaterThanOrEqualTo", L0, L0));
	}
	
	public static boolean equalTo(long a, long b) {
		return a == b;
	}

	public static boolean greaterThan(long a, long b) {
		return a > b;
	}

	public static boolean lessThan(long a, long b) {
		return a < b;
	}
	
	public static boolean notEqualTo(long a, long b) {
		return a != b;
	}

	public static boolean lessThanOrEqualTo(long a, long b) {
		return a <= b;
	}

	public static boolean greaterThanOrEqualTo(long a, long b) {
		return a >= b;
	}	
	
}
