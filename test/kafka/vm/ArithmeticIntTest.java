package kafka.vm;

import static org.junit.Assert.assertEquals;
import kafka.ir.instances.Instance;
import kafka.ir.types.Class;

import org.junit.Test;

public class ArithmeticIntTest extends VmTestBase {

	static final Class THIS = ir(ArithmeticIntTest.class);

	private Instance invoke(String name, Instance... arguments) {
		return super.invoke(THIS, name, "(II)I", arguments);
	}

	@Test
	public void testArithmetic() {
		assertEquals(ir(-10), invoke("addIntegers", I0, I_10));
		assertEquals(ir(1010), invoke("subtractIntegers", I1000, I_10));
		assertEquals(ir(-20), invoke("multiplyIntegers", I2, I_10));
		assertEquals(ir(-100), invoke("divideIntegers", I1000, I_10));
		assertEquals(ir(10), invoke("negateValues", I_10, I_10));
	}

	@Test
	public void testLogic() {
		assertEquals(I2, invoke("shiftIntegers", I3, I1));
		assertEquals(ir(3 % 2), invoke("reminderIntegers", I3, I2));
		assertEquals(ir(3 & 2), invoke("andIntegers", I3, I2));
		assertEquals(ir(1 | 2), invoke("orIntegers", I1, I2));
		assertEquals(ir(3 ^ 2), invoke("xorIntegers", I3, I2));
		assertEquals(ir(~1000), invoke("complementIntegers", I1000, I0));
	}

	public static int addIntegers(int a, int b) {
		return a + b;
	}

	public static int subtractIntegers(int a, int b) {
		return a - b;
	}

	public static int multiplyIntegers(int a, int b) {
		return a * b;
	}

	public static int divideIntegers(int a, int b) {
		return a / b;
	}	
	
	public static int negateValues(int a, int b) {
		return -a;
	}	
	
	public static int shiftIntegers(int a, int b) {
		return (a >> b) << b;
	}
	
	public static int reminderIntegers(int a, int b) {
		return a % b;
	}	
	
	public static int andIntegers(int a, int b) {
		return a & b;
	}
	
	public static int orIntegers(int a, int b) {
		return a | b;
	}
	
	public static int xorIntegers(int a, int b) {
		return a ^ b;
	}
	
	public static int complementIntegers(int a, int b) {
		return ~a;
	}
}
