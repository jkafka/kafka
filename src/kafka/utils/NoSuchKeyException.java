package kafka.utils;

public class NoSuchKeyException extends RuntimeException {
	private static final long serialVersionUID = -3100823754305174236L;
	private final Object key;

	public NoSuchKeyException(Object key) {
		this.key = key;
	}

	@Override
	public String toString() {
		return "NoSuchKeyException(" + key + ")";
	}
	
}
