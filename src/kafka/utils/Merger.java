package kafka.utils;

public interface Merger<V> {
	V merge(V left, V right);
}
