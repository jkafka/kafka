package kafka.utils;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Stack;


public class StackMap<K, V> implements Map<K, V> {

	private static class Node<K, V> {
		Map<K, V> left;
		Map<K, V> right;
		Map<K, V> parentMap;
		Node(Map<K, V> parentMap) {
			this.parentMap = parentMap;
			this.left = new HashMap<K, V>();
			this.right = new HashMap<K, V>(); 
		}
	}
	
	private Stack<Node<K, V>> stack;
	private Merger<V> merger;
	private Map<K, V> currentMap;
	
	public StackMap(Merger<V> merger) {
		this.merger = merger;
		this.stack = new Stack<Node<K, V>>();
		this.currentMap = new HashMap<K, V>();
		this.stack.push(new Node<K, V>(this.currentMap));
	}
	
	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean containsKey(Object key) {
		return currentMap.containsKey(key);
	}

	@Override
	public boolean containsValue(Object arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Set<Map.Entry<K, V>> entrySet() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Set<K> keySet() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public V get(Object key) {
		return currentMap.get(key);
	}

	@Override
	public V put(K key, V value) {
		currentMap.put(key, value);
		return null;
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public V remove(Object arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Collection<V> values() {
		// TODO Auto-generated method stub
		return null;
	}

	public void pushBranch() {
		stack.push(new Node<K, V>(this.currentMap));		
		currentMap = null;
	}

	public void lbranch() {
		currentMap = stack.peek().left; 
	}

	public void rbranch() {
		currentMap = stack.peek().right; 
	}

	public void popBranch() {
		Node<K, V> node = stack.pop();
		currentMap = node.parentMap;
		Map<K, V> left = node.left;
		Map<K, V> right = node.right;
		Set<K> lkeys = left.keySet();
		Set<K> rkeys = right.keySet();
		Set<K> pkeys = node.parentMap.keySet();

		for (K lkey : lkeys) {
			if (rkeys.contains(lkey)) {
				V value = merger.merge(left.get(lkey), right.get(lkey));
				currentMap.put(lkey, value);
			} else if (pkeys.contains(lkey)) {
				V value = merger.merge(left.get(lkey), currentMap.get(lkey));
				currentMap.put(lkey, value);
			}
		}
		
		for (K rkey : rkeys) {
			if (lkeys.contains(rkey))
				continue;
			else if (pkeys.contains(rkey)) {
				V value = merger.merge(currentMap.get(rkey), right.get(rkey));
				currentMap.put(rkey, value);
			}
		}

	}
}
