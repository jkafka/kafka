package kafka.utils;

import java.util.HashMap;

/**
 * Like a normal map but in addition makes sure that no null:s are returned
 * when the key does not exists in the map -- instead an exception is thrown.
 */
public final class NonNullMap<K, V> extends HashMap<K, V> {
	private static final long serialVersionUID = 6301851117220695090L;

	@Override
	public V get(Object key) {
		V obj = super.get(key);
		if (obj == null)
			throw new NoSuchKeyException(key);
		return obj;
	}
}
