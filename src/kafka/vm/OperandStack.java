package kafka.vm;

import kafka.ir.instances.Instance;

public class OperandStack {
	private static final int INITIAL_TOP = 0;
	// TODO: Avoid copying stack elements when passing arguments by having all
	// operand stacks share the same (big!) Instance[] array. 
	private final Instance[] elements;
	private int currentTop = INITIAL_TOP;

	public OperandStack(int maxSize) {
		elements = new Instance[maxSize];
	}

	public void push(Instance item) {
		assert item != null;
		elements[currentTop++] = item;
	}
	
	public Instance pop() {
		Instance item = elements[--currentTop];
		assert item != null;
		return item;
	}
	
	public void popTo(int numElements, Instance[] destination) {
		for (int i = 0; i < numElements; i++)
			destination[destination.length - 1 - i] = pop();
	}
	
	@Override
	public String toString() {
		StringBuffer b = new StringBuffer();
		b.append("[");
		for (int i = 0; i < currentTop; i++) {
			b.append(elements[currentTop - 1 - i]);
			if (i + 1 < currentTop)
				b.append(", ");
		}
		b.append("]");		
		return b.toString();
	}

	public Instance top() {
		return elements[currentTop - 1];
	}

	public void clear() {
		currentTop = INITIAL_TOP;
	}

}
