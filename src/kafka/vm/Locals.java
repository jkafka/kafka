package kafka.vm;

import java.util.Arrays;

import kafka.ir.instances.Instance;

public class Locals {

	private Instance[] elements;

	public Locals(int size) {
		elements = new Instance[size];
	}

	public void put(int index, Instance instance) {
		assert instance != null;
		elements[index] = instance;
	}

	public Instance get(int index) {
		Instance i = elements[index];
		assert i != null : index;
		return i;
	}

	@Override
	public String toString() {
		return "Locals(" + Arrays.toString(elements) + ")";
	}
	
}
