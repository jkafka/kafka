package kafka.vm;


import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import kafka.ir.Builder;
import kafka.ir.instances.ClassInstance;
import kafka.ir.instances.Instance;
import kafka.ir.instances.LogicInstance;
import kafka.ir.instances.NumericInstance;
import kafka.ir.instances.Primitives;
import kafka.ir.instances.Primitives.Array;
import kafka.ir.instances.Primitives.Int;
import kafka.ir.instances.Primitives.Long;
import kafka.ir.instances.Primitives.Double;
import kafka.ir.instances.Primitives.Float;
import kafka.ir.instructions.Instruction;
import kafka.ir.instructions.InstructionVisitor;
import kafka.ir.types.AbstractMethodBuilder.AConstNull;
import kafka.ir.types.AbstractMethodBuilder.Aaload;
import kafka.ir.types.AbstractMethodBuilder.Aastore;
import kafka.ir.types.AbstractMethodBuilder.Acmp;
import kafka.ir.types.AbstractMethodBuilder.Add;
import kafka.ir.types.AbstractMethodBuilder.Aload;
import kafka.ir.types.AbstractMethodBuilder.And;
import kafka.ir.types.AbstractMethodBuilder.Anewarray;
import kafka.ir.types.AbstractMethodBuilder.Areturn;
import kafka.ir.types.AbstractMethodBuilder.Arraylength;
import kafka.ir.types.AbstractMethodBuilder.Astore;
import kafka.ir.types.AbstractMethodBuilder.Athrow;
import kafka.ir.types.AbstractMethodBuilder.Baload;
import kafka.ir.types.AbstractMethodBuilder.Bastore;
import kafka.ir.types.AbstractMethodBuilder.Bipush;
import kafka.ir.types.AbstractMethodBuilder.Caload;
import kafka.ir.types.AbstractMethodBuilder.Castore;
import kafka.ir.types.AbstractMethodBuilder.Checkcast;
import kafka.ir.types.AbstractMethodBuilder.D2f;
import kafka.ir.types.AbstractMethodBuilder.D2i;
import kafka.ir.types.AbstractMethodBuilder.D2l;
import kafka.ir.types.AbstractMethodBuilder.DConst;
import kafka.ir.types.AbstractMethodBuilder.Daload;
import kafka.ir.types.AbstractMethodBuilder.Dastore;
import kafka.ir.types.AbstractMethodBuilder.Dcmpg;
import kafka.ir.types.AbstractMethodBuilder.Dcmpl;
import kafka.ir.types.AbstractMethodBuilder.Div;
import kafka.ir.types.AbstractMethodBuilder.Dload;
import kafka.ir.types.AbstractMethodBuilder.Dneg;
import kafka.ir.types.AbstractMethodBuilder.Dreturn;
import kafka.ir.types.AbstractMethodBuilder.Dstore;
import kafka.ir.types.AbstractMethodBuilder.Dup;
import kafka.ir.types.AbstractMethodBuilder.Dup2;
import kafka.ir.types.AbstractMethodBuilder.Dup2_x1;
import kafka.ir.types.AbstractMethodBuilder.Dup2_x2;
import kafka.ir.types.AbstractMethodBuilder.Dup_x1;
import kafka.ir.types.AbstractMethodBuilder.Dup_x2;
import kafka.ir.types.AbstractMethodBuilder.F2d;
import kafka.ir.types.AbstractMethodBuilder.F2i;
import kafka.ir.types.AbstractMethodBuilder.F2l;
import kafka.ir.types.AbstractMethodBuilder.FConst;
import kafka.ir.types.AbstractMethodBuilder.Faload;
import kafka.ir.types.AbstractMethodBuilder.Fastore;
import kafka.ir.types.AbstractMethodBuilder.Fcmpg;
import kafka.ir.types.AbstractMethodBuilder.Fcmpl;
import kafka.ir.types.AbstractMethodBuilder.Fload;
import kafka.ir.types.AbstractMethodBuilder.Fneg;
import kafka.ir.types.AbstractMethodBuilder.Freturn;
import kafka.ir.types.AbstractMethodBuilder.Fstore;
import kafka.ir.types.AbstractMethodBuilder.Getfield;
import kafka.ir.types.AbstractMethodBuilder.Getstatic;
import kafka.ir.types.AbstractMethodBuilder.Goto;
import kafka.ir.types.AbstractMethodBuilder.I2b;
import kafka.ir.types.AbstractMethodBuilder.I2c;
import kafka.ir.types.AbstractMethodBuilder.I2d;
import kafka.ir.types.AbstractMethodBuilder.I2f;
import kafka.ir.types.AbstractMethodBuilder.I2l;
import kafka.ir.types.AbstractMethodBuilder.I2s;
import kafka.ir.types.AbstractMethodBuilder.IConst;
import kafka.ir.types.AbstractMethodBuilder.Iaload;
import kafka.ir.types.AbstractMethodBuilder.Iastore;
import kafka.ir.types.AbstractMethodBuilder.Icmp;
import kafka.ir.types.AbstractMethodBuilder.Ifeq;
import kafka.ir.types.AbstractMethodBuilder.Ifge;
import kafka.ir.types.AbstractMethodBuilder.Ifgt;
import kafka.ir.types.AbstractMethodBuilder.Ifle;
import kafka.ir.types.AbstractMethodBuilder.Iflt;
import kafka.ir.types.AbstractMethodBuilder.Ifne;
import kafka.ir.types.AbstractMethodBuilder.Ifnonnull;
import kafka.ir.types.AbstractMethodBuilder.Ifnull;
import kafka.ir.types.AbstractMethodBuilder.Iinc;
import kafka.ir.types.AbstractMethodBuilder.Iload;
import kafka.ir.types.AbstractMethodBuilder.Ineg;
import kafka.ir.types.AbstractMethodBuilder.Instanceof;
import kafka.ir.types.AbstractMethodBuilder.Invokeinterface;
import kafka.ir.types.AbstractMethodBuilder.Invokespecial;
import kafka.ir.types.AbstractMethodBuilder.Invokestatic;
import kafka.ir.types.AbstractMethodBuilder.Invokevirtual;
import kafka.ir.types.AbstractMethodBuilder.Ireturn;
import kafka.ir.types.AbstractMethodBuilder.Istore;
import kafka.ir.types.AbstractMethodBuilder.Jsr;
import kafka.ir.types.AbstractMethodBuilder.L2d;
import kafka.ir.types.AbstractMethodBuilder.L2f;
import kafka.ir.types.AbstractMethodBuilder.L2i;
import kafka.ir.types.AbstractMethodBuilder.LConst;
import kafka.ir.types.AbstractMethodBuilder.Laload;
import kafka.ir.types.AbstractMethodBuilder.Lastore;
import kafka.ir.types.AbstractMethodBuilder.Lcmp;
import kafka.ir.types.AbstractMethodBuilder.Ldc;
import kafka.ir.types.AbstractMethodBuilder.Lload;
import kafka.ir.types.AbstractMethodBuilder.Lneg;
import kafka.ir.types.AbstractMethodBuilder.Lookupswitch;
import kafka.ir.types.AbstractMethodBuilder.Lreturn;
import kafka.ir.types.AbstractMethodBuilder.Lstore;
import kafka.ir.types.AbstractMethodBuilder.Monitorenter;
import kafka.ir.types.AbstractMethodBuilder.Monitorexit;
import kafka.ir.types.AbstractMethodBuilder.Mul;
import kafka.ir.types.AbstractMethodBuilder.Multianewarray;
import kafka.ir.types.AbstractMethodBuilder.New;
import kafka.ir.types.AbstractMethodBuilder.Newarray;
import kafka.ir.types.AbstractMethodBuilder.Nop;
import kafka.ir.types.AbstractMethodBuilder.Or;
import kafka.ir.types.AbstractMethodBuilder.Pop;
import kafka.ir.types.AbstractMethodBuilder.Pop2;
import kafka.ir.types.AbstractMethodBuilder.Putfield;
import kafka.ir.types.AbstractMethodBuilder.Putstatic;
import kafka.ir.types.AbstractMethodBuilder.Rem;
import kafka.ir.types.AbstractMethodBuilder.Ret;
import kafka.ir.types.AbstractMethodBuilder.Returnv;
import kafka.ir.types.AbstractMethodBuilder.Saload;
import kafka.ir.types.AbstractMethodBuilder.Sastore;
import kafka.ir.types.AbstractMethodBuilder.Shl;
import kafka.ir.types.AbstractMethodBuilder.Shr;
import kafka.ir.types.AbstractMethodBuilder.Sipush;
import kafka.ir.types.AbstractMethodBuilder.Strconst;
import kafka.ir.types.AbstractMethodBuilder.Sub;
import kafka.ir.types.AbstractMethodBuilder.Swap;
import kafka.ir.types.AbstractMethodBuilder.Tableswitch;
import kafka.ir.types.AbstractMethodBuilder.Typeconst;
import kafka.ir.types.AbstractMethodBuilder.Ushr;
import kafka.ir.types.AbstractMethodBuilder.Xor;
import kafka.ir.types.*;
import kafka.ir.types.Class;

public class Vm implements InstructionVisitor {

	private static final int GREATER_THAN = 1;
	private static final int EQUAL_TO = 0;
	private static final int LESS_THAN = -1;
	
	private OperandStack stack;
	private Locals locals;
	private Instance returnValue;
	private final Builder builder;
	private int nextLabel;
	private ClassInstance raiseException;
	private final Map<Class, Object> natives;

	public Vm(Map<Class, Object> natives,
			  Builder builder) {
		this.natives = natives;
		this.builder = builder;
	}

	private static void log(Object o) {
		//System.err.println(o);
	}

	public Instance invoke(Method method, Instance[] params) {
		log("invoke: " + method);
		log(method.codeString());

		if (method.isNative())
			executeNativeMethod(method, params);
		else
			executeMethod(method, params);
				
		if (raiseException != null) {
			log("exception raised, " + method + ": " + raiseException);
			return new ExceptionOccured(raiseException);
		}

		log("returned, " + method + ": " + returnValue);
		return returnValue;
	}

	private void executeNativeMethod(Method method, Instance[] params) {
		Object object = natives.get(method.owner());
		if (object == null) {
			System.out.println(method.owner());
			System.out.println(natives);
			throw new RuntimeException("Unimplemented native: " +
					method.owner() + "." + method);
		}
		try {
			java.lang.reflect.Method method2 =
					object.getClass().getMethod(method.reference().name(), Instance[].class);
			returnValue = (Instance)method2.invoke(object, (Object)params);
		} catch (SecurityException e) {
			throw new RuntimeException(e);
		} catch (NoSuchMethodException e) {
			throw new RuntimeException(e);
		} catch (IllegalArgumentException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		} catch (InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}

	private void executeMethod(Method method, Instance[] params) {
		stack = new OperandStack(method.maxStack());
		locals = initLocals(method, params);

		int currentLabel = 0;
		Iterable<Instruction> currentBasicBlock = method.basicblocks().get(currentLabel);
		while (returnValue == null & raiseException == null) {
			nextLabel = -1;
			for (Instruction i : currentBasicBlock) {
				log("  " + i + "\t" + stack + "\t" + locals);
				i.visit(this);
				
				if (raiseException != null) {
					nextLabel = checkForCatchBlock(currentLabel, method.exceptionTable());
					if (nextLabel != -1)
						raiseException = null;
				}

				if (nextLabel != -1) {
					currentLabel = nextLabel;
					currentBasicBlock = method.basicblocks().get(currentLabel);
					break;
				}

			}
			
			if (nextLabel == -1 & returnValue == null & raiseException == null) // fall through to next bb
				currentBasicBlock = method.basicblocks().get(++currentLabel);
		}
	}
	
	int checkForCatchBlock(int currentLabel, TryCatchBlock[] exceptionTable) {
		if (exceptionTable == null)
			return -1;
		for (TryCatchBlock block : exceptionTable) {
			log(block);
			if (block.contains(currentLabel) &
					raiseException.type().isSubclass(block.caughtException())) {
				return block.handlerLabel();
			}
		}
		return -1;
	}

	private static Locals initLocals(Method method, Instance[] params) {
		Locals initLocals = new Locals(method.maxLocals() + (method.isStatic() ? 0 : 1));

		int localIdx = 0;
		int paramIdx = 0;
		if (!method.isStatic())
			initLocals.put(localIdx++, params[paramIdx++]);
		for (; paramIdx < params.length; paramIdx++) {
			Instance param = params[paramIdx];
			initLocals.put(localIdx, param);
			if (occupiesTwoSlots(param))
				localIdx += 2;
			else
				localIdx += 1;
		}
		return initLocals;
	}

	/**
	 * Checks if param takes two local variable slots.
	 */
	private static boolean occupiesTwoSlots(Instance param) {
		return param instanceof Primitives.Long | param instanceof Primitives.Double;
	}

	@Override
	public void visit(Nop instruction) {
	}

	@Override
	public void visit(AConstNull instruction) {
		stack.push(builder.buildNull());
	}

	@Override
	public void visit(IConst instruction) {
		stack.push(instruction.value());
	}

	@Override
	public void visit(LConst instruction) {
		stack.push(instruction.value());
	}

	@Override
	public void visit(FConst instruction) {
		stack.push(instruction.value());
	}

	@Override
	public void visit(DConst instruction) {
		stack.push(instruction.value());
	}
	

	@Override
	public void visit(Typeconst instruction) {
		throw new RuntimeException(); // TODO: Implement
	}

	@Override
	public void visit(Aload instruction) {
		stack.push(locals.get(instruction.var()));
	}

	@Override
	public void visit(Astore instruction) {
		locals.put(instruction.var(), stack.pop());
	}

	@Override
	public void visit(Pop instruction) {
		stack.pop();
	}

	@Override
	public void visit(Pop2 instruction) {
		throw new RuntimeException(); // TODO: Implement
	}

	@Override
	public void visit(Dup instruction) {
		stack.push(stack.top());
	}

	@Override
	public void visit(Dup_x1 instruction) {
		throw new RuntimeException(); // TODO: Implement
	}

	@Override
	public void visit(Dup_x2 instruction) {
		throw new RuntimeException(); // TODO: Implement
	}

	@Override
	public void visit(Dup2 instruction) {
		throw new RuntimeException(); // TODO: Implement
	}

	@Override
	public void visit(Dup2_x1 instruction) {
		throw new RuntimeException(); // TODO: Implement
	}

	@Override
	public void visit(Dup2_x2 instruction) {
		throw new RuntimeException(); // TODO: Implement
	}

	@Override
	public void visit(Swap instruction) {
		throw new RuntimeException(); // TODO: Implement
	}

	@Override
	public void visit(Add instruction) {
		NumericInstance rhs = (NumericInstance) stack.pop();
		NumericInstance lhs = (NumericInstance) stack.pop();
		stack.push(lhs.plus(rhs, builder));
	}

	@Override
	public void visit(Sub instruction) {
		NumericInstance rhs = (NumericInstance) stack.pop();
		NumericInstance lhs = (NumericInstance) stack.pop();
		stack.push(lhs.minus(rhs, builder));
	}
	
	@Override
	public void visit(Mul instruction) {
		NumericInstance rhs = (NumericInstance) stack.pop();
		NumericInstance lhs = (NumericInstance) stack.pop();
		stack.push(lhs.times(rhs, builder));		
	}

	@Override
	public void visit(Div instruction) {
		NumericInstance rhs = (NumericInstance) stack.pop();
		NumericInstance lhs = (NumericInstance) stack.pop();
		stack.push(lhs.divide(rhs, builder));
	}

	@Override
	public void visit(Rem instruction) {
		LogicInstance rhs = (LogicInstance) stack.pop();
		LogicInstance lhs = (LogicInstance) stack.pop();
		stack.push(lhs.reminder(rhs, builder));
	}

	@Override
	public void visit(Ineg instruction) {
		NumericInstance operand = (NumericInstance) stack.pop();
		stack.push(operand.negate(builder));
	}

	@Override
	public void visit(Lneg instruction) {
		NumericInstance operand = (NumericInstance) stack.pop();
		stack.push(operand.negate(builder));
	}

	@Override
	public void visit(Dneg instruction) {
		NumericInstance operand = (NumericInstance) stack.pop();
		stack.push(operand.negate(builder));
	}
	
	@Override
	public void visit(Fneg instruction) {
		NumericInstance operand = (NumericInstance) stack.pop();
		stack.push(operand.negate(builder));
	}

	@Override
	public void visit(Shl instruction) {
		LogicInstance rhs = (LogicInstance) stack.pop();
		LogicInstance lhs = (LogicInstance) stack.pop();
		stack.push(lhs.leftShift(rhs, builder));
	}

	@Override
	public void visit(Shr instruction) {
		LogicInstance rhs = (LogicInstance) stack.pop();
		LogicInstance lhs = (LogicInstance) stack.pop();
		stack.push(lhs.rightShift(rhs, builder));
	}

	@Override
	public void visit(Ushr instruction) {
		LogicInstance rhs = (LogicInstance) stack.pop();
		LogicInstance lhs = (LogicInstance) stack.pop();
		stack.push(lhs.rightShift(rhs, builder));
	}

	@Override
	public void visit(And instruction) {
		LogicInstance rhs = (LogicInstance) stack.pop();
		LogicInstance lhs = (LogicInstance) stack.pop();
		stack.push(lhs.logicAnd(rhs, builder));
	}

	@Override
	public void visit(Or instruction) {
		LogicInstance rhs = (LogicInstance) stack.pop();
		LogicInstance lhs = (LogicInstance) stack.pop();
		stack.push(lhs.logicOr(rhs, builder));
	}

	@Override
	public void visit(Xor instruction) {
		LogicInstance rhs = (LogicInstance) stack.pop();
		LogicInstance lhs = (LogicInstance) stack.pop();
		stack.push(lhs.logicXor(rhs, builder));
	}

	@Override
	public void visit(I2l instruction) {
		stack.push(tolong(stack.pop()));
	}

	@Override
	public void visit(I2f instruction) {
		stack.push(tofloat(stack.pop()));
	}

	@Override
	public void visit(I2d instruction) {
		stack.push(todouble(stack.pop()));
	}

	@Override
	public void visit(L2i instruction) {
		stack.push(toint(stack.pop()));
	}

	@Override
	public void visit(L2f instruction) {
		stack.push(tofloat(stack.pop()));
	}

	@Override
	public void visit(L2d instruction) {
		stack.push(todouble(stack.pop()));
	}

	@Override
	public void visit(F2i instruction) {
		stack.push(toint(stack.pop()));
	}

	@Override
	public void visit(F2l instruction) {
		stack.push(tolong(stack.pop()));
	}

	@Override
	public void visit(F2d instruction) {
		stack.push(todouble(stack.pop()));
	}

	@Override
	public void visit(D2i instruction) {
		stack.push(toint(stack.pop()));
	}

	@Override
	public void visit(D2l instruction) {
		stack.push(tolong(stack.pop()));
	}

	@Override
	public void visit(D2f instruction) {
		stack.push(tofloat(stack.pop()));
	}

	@Override
	public void visit(I2b instruction) {
		stack.push(tobyte(stack.pop()));
	}

	@Override
	public void visit(I2c instruction) {
		stack.push(tochar(stack.pop()));
	}

	@Override
	public void visit(I2s instruction) {
		stack.push(toshort(stack.pop()));
	}

	@Override
	public void visit(Acmp instruction) {
		Instance value1 = stack.pop();
		Instance value2 = stack.pop();
		if (value1 == value2)
			stack.push(builder.buildInt(EQUAL_TO));
		else
			stack.push(builder.buildInt(LESS_THAN)); // Use LESS_THAN to represent "not equals" for references		
	}

	@Override
	public void visit(Icmp instruction) {
		Int value1 = (Int) stack.pop();
		Int value2 = (Int) stack.pop();
		if (value2.equalTo(value1))
			stack.push(builder.buildInt(EQUAL_TO));
		else if (value2.greaterThan(value1))
			stack.push(builder.buildInt(GREATER_THAN));
		else // if (value2.lessThan(value1))
			stack.push(builder.buildInt(LESS_THAN));		
	}
	
	@Override
	public void visit(Lcmp instruction) {
		Long value1 = (Long) stack.pop();
		Long value2 = (Long) stack.pop();
		if (value2.equalTo(value1))
			stack.push(builder.buildInt(EQUAL_TO));
		else if (value2.greaterThan(value1))
			stack.push(builder.buildInt(GREATER_THAN));
		else // if (value2.lessThan(value1))
			stack.push(builder.buildInt(LESS_THAN));
	}

	@Override
	public void visit(Fcmpl instruction) {
		Float value1 = (Float) stack.pop();
		Float value2 = (Float) stack.pop();
		if (value1.isNan() | value2.isNan())
			stack.push(builder.buildInt(GREATER_THAN));
		else if (value2.equalTo(value1))
			stack.push(builder.buildInt(EQUAL_TO));
		else if (value2.greaterThan(value1))
			stack.push(builder.buildInt(GREATER_THAN));
		else // if (value2.lessThan(value1))
			stack.push(builder.buildInt(LESS_THAN));

	}

	@Override
	public void visit(Fcmpg instruction) {
		Float value1 = (Float) stack.pop();
		Float value2 = (Float) stack.pop();
		if (value1.isNan() | value2.isNan())
			stack.push(builder.buildInt(LESS_THAN));
		else if (value2.equalTo(value1))
			stack.push(builder.buildInt(EQUAL_TO));
		else if (value2.greaterThan(value1))
			stack.push(builder.buildInt(GREATER_THAN));
		else // if (value2.lessThan(value1))
			stack.push(builder.buildInt(LESS_THAN));
	}

	@Override
	public void visit(Dcmpl instruction) {
		Double value1 = (Double) stack.pop();
		Double value2 = (Double) stack.pop();
		if (value1.isNan() | value2.isNan())
			stack.push(builder.buildInt(GREATER_THAN));
		else if (value2.equalTo(value1))
			stack.push(builder.buildInt(EQUAL_TO));
		else if (value2.greaterThan(value1))
			stack.push(builder.buildInt(GREATER_THAN));
		else // if (value2.lessThan(value1))
			stack.push(builder.buildInt(LESS_THAN));
	}

	@Override
	public void visit(Dcmpg instruction) {
		Double value1 = (Double) stack.pop();
		Double value2 = (Double) stack.pop();
		if (value1.isNan() | value2.isNan())
			stack.push(builder.buildInt(LESS_THAN));
		else if (value2.equalTo(value1))
			stack.push(builder.buildInt(EQUAL_TO));
		else if (value2.greaterThan(value1))
			stack.push(builder.buildInt(GREATER_THAN));
		else // if (value2.lessThan(value1))
			stack.push(builder.buildInt(LESS_THAN));
	}

	@Override
	public void visit(Ireturn instruction) {
		// TOS should be int, short, or byte here.
		returnValue = stack.pop();
	}

	@Override
	public void visit(Lreturn instruction) {
		returnValue = tolong(stack.pop());
	}
	
	@Override
	public void visit(Returnv instruction) {
		returnValue = builder.buildVoid();
	}

	@Override
	public void visit(Arraylength instruction) {
		stack.push(((Array)stack.pop()).size());	
	}

	@Override
	public void visit(Athrow instruction) {
		raiseException = (ClassInstance)stack.pop();
		stack.clear();
		stack.push(raiseException);
	}

	@Override
	public void visit(Monitorenter instruction) {
		throw new RuntimeException(); // TODO: Implement
	}

	@Override
	public void visit(Monitorexit instruction) {
		throw new RuntimeException(); // TODO: Implement
	}

	@Override
	public void visit(Bipush instruction) {
		stack.push(instruction.operand());
	}

	@Override
	public void visit(Sipush instruction) {
		stack.push(instruction.operand());
	}

	@Override
	public void visit(Newarray instruction) {
		stack.push(builder.buildArray(instruction.type(), (Primitives.Int)stack.pop()));
	}

	@Override
	public void visit(Iload instruction) {
		stack.push(toint(locals.get(instruction.var())));
	}

	@Override
	public void visit(Ret instruction) {
		throw new RuntimeException(); // TODO: Implement
	}

	@Override
	public void visit(New instruction) {
		stack.push(instruction.type().instantiate(builder));
	}

	@Override
	public void visit(Anewarray instruction) {
		stack.push(builder.buildArray(instruction.type(), (Primitives.Int)stack.pop()));
	}

	@Override
	public void visit(Checkcast instruction) {
		ReferenceType type = (ReferenceType) stack.top().type();
		// TODO: Make Checkcast.type return a ReferenceType instead.
		if (!type.isSubclass((ReferenceType) instruction.type()))
			raiseException = builder.buildClass(ClassCastException.class.getCanonicalName()).instantiate(builder);
	}

	@Override
	public void visit(Instanceof instruction) {
		ReferenceType lhs = (ReferenceType) stack.pop().type();
		ReferenceType rhs = (ReferenceType) instruction.type();
		stack.push(builder.buildInt(lhs.isSubclass(rhs) ? 1 : 0));
	}

	@Override
	public void visit(Getstatic instruction) {
		Instance value = instruction.owner().staticField(instruction.name()).read();
		stack.push(value);
	}

	@Override
	public void visit(Putstatic instruction) {
		instruction.owner().staticField(instruction.name()).write(stack.pop());
	}

	@Override
	public void visit(Getfield instruction) {
		ClassInstance owner = (ClassInstance) stack.pop();
		Instance value = owner.field(instruction.name()).read();
		stack.push(value);
	}

	@Override
	public void visit(Putfield instruction) {
		Instance value = stack.pop();
		ClassInstance owner = (ClassInstance) stack.pop();
		owner.field(instruction.name()).write(value);
	}
	
	@Override
	public void visit(Invokevirtual instruction) {
		invokeVirtualMethod(instruction.method());
	}

	private void invokeVirtualMethod(MethodReference methodRef) {
		int numargs = methodRef.type().arguments().length;
		Instance[] arguments = new Instance[numargs + 1];
		stack.popTo(numargs + 1, arguments);
		Instance target = arguments[0]; // Invocation target, a.k.a 'this'.
		Method method = ((ReferenceType)target.type()).method(methodRef);
		Instance result = new Vm(natives, builder).invoke(method, arguments);
		log("invokation result: " + result);
		if (result != builder.buildVoid()) 
			stack.push(result);
	}

	@Override
	public void visit(Invokespecial instruction) {
		MethodReference methodRef = instruction.method();
		int numargs = methodRef.type().arguments().length;
		Instance[] arguments = new Instance[numargs + 1];
		stack.popTo(numargs + 1, arguments);
		
		Method method = instruction.owner().method(methodRef);
		Instance result = new Vm(natives, builder).invoke(method, arguments);
		log("invokation result: " + result);
		if (result != builder.buildVoid()) 
			stack.push(result);
	}

	@Override
	public void visit(Invokestatic instruction) {
		MethodReference methodRef = instruction.method();
		int numargs = methodRef.type().arguments().length;
		Instance[] arguments = new Instance[numargs];
		stack.popTo(numargs, arguments);
		Method method = instruction.owner().method(methodRef);
		Instance result = new Vm(natives, builder).invoke(method, arguments);
		log("invokation result: " + result);
		if (result != builder.buildVoid()) 
			stack.push(result);
	}

	@Override
	public void visit(Invokeinterface instruction) {
		invokeVirtualMethod(instruction.method());		
	}

	@Override
	public void visit(Ifeq instruction) {
		long cmp = ((Primitives.Int) stack.pop()).value();
		if (cmp == EQUAL_TO)
			nextLabel = instruction.label();
	}

	@Override
	public void visit(Ifne instruction) {
		long cmp = ((Primitives.Int) stack.pop()).value();
		if (cmp != EQUAL_TO)
			nextLabel = instruction.label();
	}

	@Override
	public void visit(Iflt instruction) {
		long cmp = ((Primitives.Int) stack.pop()).value();
		if (cmp == LESS_THAN)
			nextLabel = instruction.label();
	}

	@Override
	public void visit(Ifge instruction) {
		long cmp = ((Primitives.Int) stack.pop()).value();
		if (cmp == GREATER_THAN | cmp == EQUAL_TO)
			nextLabel = instruction.label();
	}

	@Override
	public void visit(Ifgt instruction) {
		long cmp = ((Primitives.Int) stack.pop()).value();
		if (cmp == GREATER_THAN)
			nextLabel = instruction.label();
	}

	@Override
	public void visit(Ifle instruction) {
		long cmp = ((Primitives.Int) stack.pop()).value();
		if (cmp == LESS_THAN | cmp == EQUAL_TO)
			nextLabel = instruction.label();
	}

	@Override
	public void visit(Goto instruction) {
		nextLabel = instruction.label();
	}

	@Override
	public void visit(Jsr instruction) {
		throw new RuntimeException(); // TODO: Implement
	}

	@Override
	public void visit(Ifnull instruction) {
		Instance ref = stack.pop();
		if (ref == builder.buildNull())
			nextLabel = instruction.label();
	}

	@Override
	public void visit(Ifnonnull instruction) {
		Instance ref = stack.pop();
		if (ref != builder.buildNull())
			nextLabel = instruction.label();
	}

	@Override
	public void visit(Ldc instruction) {
		throw new RuntimeException(instruction.toString()); // TODO: Implement
	}

	@Override
	public void visit(Iinc instruction) {
		Int i = (Int) locals.get(instruction.var());
		locals.put(instruction.var(),
				i.plus(builder.buildInt(instruction.increment()), builder));
	}

	@Override
	public void visit(Tableswitch instruction) {
		int val = (int)((Int)stack.pop()).value();
		if (val < instruction.min() | val > instruction.max())
			nextLabel = instruction.dflt();
		else
			nextLabel = instruction.labels()[val - instruction.min()];
	}

	@Override
	public void visit(Lookupswitch instruction) {
		Int item = (Int)stack.pop();
		int[] labels = instruction.labels();
		int[] keys = instruction.keys();
		for (int i = 0; i < labels.length; i++) {
			if (keys[i] == item.value()) {
				nextLabel = labels[i];
				return;
			}
		}
		nextLabel = instruction.dflt();
	}

	@Override
	public void visit(Multianewarray instruction) {
		throw new RuntimeException(); // TODO: Implement
	}

	@Override
	public void visit(Strconst instruction) {
		throw new RuntimeException(); // TODO: Implement
	}
	
	@Override
	public void visit(Iaload instruction) {
		Primitives.Int index = (Primitives.Int) stack.pop();
		Array array = (Array) stack.pop();
		stack.push(array.load(index));	
	}

	@Override
	public void visit(Laload instruction) {
		Primitives.Int index = (Primitives.Int) stack.pop();
		Array array = (Array) stack.pop();
		stack.push(array.load(index));	
	}

	@Override
	public void visit(Faload instruction) {
		Primitives.Int index = (Primitives.Int) stack.pop();
		Array array = (Array) stack.pop();
		stack.push(array.load(index));		
	}

	@Override
	public void visit(Daload instruction) {
		Primitives.Int index = (Primitives.Int) stack.pop();
		Array array = (Array) stack.pop();
		stack.push(array.load(index));	
	}

	@Override
	public void visit(Aaload instruction) {
		Primitives.Int index = (Primitives.Int) stack.pop();
		Array array = (Array) stack.pop();
		stack.push(array.load(index));	
	}

	@Override
	public void visit(Baload instruction) {
		Primitives.Int index = (Primitives.Int) stack.pop();
		Array array = (Array) stack.pop();
		stack.push(array.load(index));	
	}

	@Override
	public void visit(Caload instruction) {
		Primitives.Int index = (Primitives.Int) stack.pop();
		Array array = (Array) stack.pop();
		stack.push(array.load(index));		
	}

	@Override
	public void visit(Saload instruction) {
		Primitives.Int index = (Primitives.Int) stack.pop();
		Array array = (Array) stack.pop();
		stack.push(array.load(index));		
	}

	@Override
	public void visit(Iastore instruction) {
		Instance value = stack.pop();
		Primitives.Int index = (Primitives.Int) stack.pop();
		Array array = (Array) stack.pop();
		array.store(index, value);	
	}

	@Override
	public void visit(Lastore instruction) {
		Instance value = stack.pop();
		Primitives.Int index = (Primitives.Int) stack.pop();
		Array array = (Array) stack.pop();
		array.store(index, value);	
	}

	@Override
	public void visit(Fastore instruction) {
		Instance value = stack.pop();
		Primitives.Int index = (Primitives.Int) stack.pop();
		Array array = (Array) stack.pop();
		array.store(index, value);
	}

	@Override
	public void visit(Dastore instruction) {
		Instance value = stack.pop();
		Primitives.Int index = (Primitives.Int) stack.pop();
		Array array = (Array) stack.pop();
		array.store(index, value);			
	}

	@Override
	public void visit(Aastore instruction) {
		Instance value = stack.pop();
		Primitives.Int index = (Primitives.Int) stack.pop();
		Array array = (Array) stack.pop();

		ReferenceType type = (ReferenceType) value.type();
		assert type.isSubclass((ReferenceType)array.type());
		
		array.store(index, value);			
	}

	@Override
	public void visit(Bastore instruction) {
		Instance value = tobyte(stack.pop());
		Primitives.Int index = (Primitives.Int) stack.pop();
		Array array = (Array) stack.pop();
		array.store(index, value);		
	}

	@Override
	public void visit(Castore instruction) {
		Instance value = tochar(stack.pop());
		Primitives.Int index = (Primitives.Int) stack.pop();
		Array array = (Array) stack.pop();
		array.store(index, value);	
	}

	@Override
	public void visit(Sastore instruction) {
		Instance value = toshort(stack.pop());
		Primitives.Int index = (Primitives.Int) stack.pop();
		Array array = (Array) stack.pop();
		array.store(index, value);	
	}

	@Override
	public void visit(Freturn instruction) {
		returnValue = tofloat(stack.pop());
	}

	@Override
	public void visit(Dreturn instruction) {
		returnValue = todouble(stack.pop());	
	}

	@Override
	public void visit(Areturn instruction) {
		returnValue =stack.pop();
	}

	@Override
	public void visit(Lload instruction) {
		stack.push(tolong(locals.get(instruction.var())));
	}

	@Override
	public void visit(Fload instruction) {
		stack.push(locals.get(instruction.var()));
	}

	@Override
	public void visit(Dload instruction) {
		stack.push(locals.get(instruction.var()));
	}

	@Override
	public void visit(Istore instruction) {
		locals.put(instruction.var(), toint(stack.pop()));
	}
	
	@Override
	public void visit(Lstore instruction) {
		locals.put(instruction.var(), tolong(stack.pop()));		
	}

	@Override
	public void visit(Fstore instruction) {
		locals.put(instruction.var(), tofloat(stack.pop()));		
	}

	@Override
	public void visit(Dstore instruction) {
		locals.put(instruction.var(), todouble(stack.pop()));		
	}

	
	// -- Helper functions below -- //
	
	
	private Instance toint(Instance value) {
		return ((NumericInstance)value).toInt(builder);
	}

	private Instance tolong(Instance value) {
		return ((NumericInstance)value).toLong(builder);
	}

	private Instance todouble(Instance value) {
		return ((NumericInstance)value).toDouble(builder);
	}

	private Instance tofloat(Instance value) {
		return ((NumericInstance)value).toFloat(builder);
	}

	private Instance toshort(Instance value) {
		return ((NumericInstance)value).toShort(builder);
	}

	private Instance tobyte(Instance value) {
		return ((NumericInstance)value).toByte(builder);
	}

	private Instance tochar(Instance value) {
		return ((NumericInstance)value).toChar(builder);
	}

}
