package kafka.vm;

import kafka.ir.instances.ClassInstance;
import kafka.ir.instances.Instance;
import kafka.ir.types.Type;

/**
 * Special value representing that a method threw an exception.
 */
public class ExceptionOccured implements Instance {

	private final ClassInstance exception;

	public ExceptionOccured(ClassInstance exception) {
		this.exception = exception;
	}

	@Override
	public Type type() {
		throw new AssertionError();
	}

	public ClassInstance exception() {
		return exception;
	}

	@Override
	public String toString() {
		return "ExceptionOccured(" + exception + ")";
	}
	
	

}
