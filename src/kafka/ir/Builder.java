package kafka.ir;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import kafka.ir.instances.Instance;
import kafka.ir.instances.Primitives;
import kafka.ir.types.Class;
import kafka.ir.types.ClassBuilder;
import kafka.ir.types.FunctionType;
import kafka.ir.types.Method;
import kafka.ir.types.MethodReference;
import kafka.ir.types.PrimitiveTypes;
import kafka.ir.types.RecursiveClass;
import kafka.ir.types.SimpleClass;
import kafka.ir.types.Type;
import kafka.utils.NoSuchKeyException;
import kafka.vm.Vm;

import org.objectweb.asm.ClassReader;


/**
 * Builds instances of immutable objects, e.g., methods, classes, integers, etc. 
 * This class is stateless (except for caching).
 */
public class Builder {
	private Map<String, Type> typeCache = new HashMap<String, Type>();
	private Map<String, RecursiveClass> proxies = new HashMap<String, RecursiveClass>();
	private Set<String> beingBuilt = new HashSet<String>();

	private Map<String, MethodReference> methodRefCache = new HashMap<String, MethodReference>();
	private Map<String, FunctionType> functionCache = new HashMap<String, FunctionType>();

	/**
	 *  List of classes for which the class constructor (the static { }-block)
	 *  have not yet been executed.
	 */
	private List<SimpleClass> uninitializedClasses = new ArrayList<SimpleClass>();

	public void runStaticInitializers(Map<Class, Object> natives) {
		ArrayList<SimpleClass> toRun = new ArrayList<SimpleClass>(uninitializedClasses);
		uninitializedClasses.clear();

		MethodReference cinitRef = buildMethodReference("<clinit>", "()V");
		Instance[] args = new Instance[0];
		for (SimpleClass cls : toRun) {
			try {
				//System.out.println(cls.codeString());
				Method cinit = cls.method(cinitRef);
				new Vm(natives, this).invoke(cinit, args);
			} catch (NoSuchKeyException ex) {
			}
		}
	}

	public MethodReference buildMethodReference(String methodName,
			String descriptor) {
		String key = methodName + descriptor;
		MethodReference ref = methodRefCache.get(key);
		if (ref == null) {
			ref = new MethodReference(methodName, buildFunctionType(descriptor));
			methodRefCache.put(key, ref);
		}
		return ref;
	}
	
	public Class buildClass(String internalClassName) {
		if (internalClassName.charAt(0) != 'L')
			internalClassName = "L" + internalClassName + ";";
		// TODO: For type safety, there should be a separate map for Class:es... but is it worth it? 
		Class result = (Class)typeCache.get(internalClassName);
		if (result == null) {
			result = doBuildClass(internalClassName);
			typeCache.put(internalClassName, result);
		}
		return result;
	}
	
	public Type buildType(String internalTypeName) {
		Type result = typeCache.get(internalTypeName);
		if (result == null) {
			result = doBuildType(internalTypeName);
			typeCache.put(internalTypeName, result);
		}
		return result;
	}

	public FunctionType buildFunctionType(String descriptor) {
		FunctionType type = functionCache.get(descriptor);
		if (type == null) {
			type = doBuildFunctionType(descriptor);
			functionCache.put(descriptor, type);
		}
		return type;
	}

	private Type doBuildType(String internalName) {
		char first = internalName.charAt(0);
		if (first == 'L')
			return doBuildClass(internalName);
		else if (first == 'Z')
			return new PrimitiveTypes.Boolean();	
		else if (first == 'C')
			return new PrimitiveTypes.Char();
		else if (first == 'B')
			return new PrimitiveTypes.Byte();
		else if (first == 'S')
			return new PrimitiveTypes.Short();		
		else if (first == 'I')
			return new PrimitiveTypes.Int();		
		else if (first == 'F')
			return new PrimitiveTypes.Float();
		else if (first == 'D')
			return new PrimitiveTypes.Double();			
		else if (first == 'J')
			return new PrimitiveTypes.Long();
		else if (first == 'V')
			return new PrimitiveTypes.VoidType();
		else if (first == '[')
			return new PrimitiveTypes.Array(buildType(internalName.substring(1)));
		// Let's try to build it as a normal class.
		return doBuildClass("L" + internalName + ";");
	}

	private Class doBuildClass(String internalName) {
		if (beingBuilt.contains(internalName)) {
			RecursiveClass proxy = proxies.get(internalName);
			if (proxy == null) {
				proxy = new RecursiveClass(stripName(internalName));
				proxies.put(internalName, proxy);
			}
				return proxy;
		}
		beingBuilt.add(internalName);
		SimpleClass result = buildSimpleClass(internalName);
		beingBuilt.remove(internalName);
		RecursiveClass proxy = proxies.get(internalName);
		if (proxy != null) {
			proxy.init(result);
			proxies.remove(internalName);
			return proxy;
		}
		return result;
	}


	private SimpleClass buildSimpleClass(String internalName) {	
		ClassReader cr;
		try {
			cr = new ClassReader(stripName(internalName));
		} catch (IOException e) {
			throw new RuntimeException("class not found:" + internalName); // TODO: Need better error handling.
		}
		ClassBuilder classBuilder = new ClassBuilder(this);
		cr.accept(classBuilder, ClassReader.SKIP_DEBUG | ClassReader.EXPAND_FRAMES);
		
		SimpleClass result = classBuilder.result();
		
		uninitializedClasses.add(result);
			
		return result;
	}

	private String stripName(String internalName) {
		// Skip leading 'L' and trailing ';'
		assert internalName.charAt(0) == 'L';
		return internalName.substring(1, internalName.length() - 1);
	}

	private FunctionType doBuildFunctionType(String descriptor) {
		assert descriptor.charAt(0) == '(';
		ArrayList<Type> args = new ArrayList<Type>();
		
		int currentIndex = 1; // start at 1 since leading parenthesis is not important.
		for (; currentIndex < descriptor.length();) {
			int startIndex = currentIndex;
			while (descriptor.charAt(currentIndex) == '[')
				currentIndex++;			
				
			char first = descriptor.charAt(currentIndex);
			if (first == 'C' || first == 'D' || first == 'F' || first == 'I' || first == 'J' || first == 'S' || first == 'Z' || first == 'B') {
				args.add(buildType(String.valueOf(first)));
				currentIndex += 1;
			} else if (first == 'L') {
				while (descriptor.charAt(currentIndex) != ';')
					currentIndex += 1;
				currentIndex += 1;
				args.add(buildType(descriptor.substring(startIndex, currentIndex)));
			} else {
				assert first == ')' : first;
				break;
			}
		}

		Type returnType = buildType(descriptor.substring(currentIndex + 1)); // Skip closing parenthesis.
		return new FunctionType(returnType, args.toArray(new Type[] {}));
	}

	
	// --- instance building functions ---
	public final Primitives.Array buildArray(Type arrayType, Primitives.Int numElements) {
		return new Primitives.Array(arrayType, numElements);
		
	}

	private Map<Short, Primitives.Short> shortCache = new HashMap<Short, Primitives.Short>(); 
	public Primitives.Short buildShort(short value) {
		Primitives.Short result = shortCache.get(value);
		if (result == null) {
			result = new Primitives.Short(value);
			shortCache.put(value, result);
		}
		return result;
	}

	private Map<Integer, Primitives.Int> integersCache = new HashMap<Integer, Primitives.Int>(); 
	public Primitives.Int buildInt(int value) {
		Primitives.Int result = integersCache.get(value);
		if (result == null) {
			result = new Primitives.Int(value);
			integersCache.put(value, result);
		}
		return result;
	}

	private Map<Long, Primitives.Long> longCache = new HashMap<Long, Primitives.Long>(); 
	public Primitives.Long buildLong(long value) {
		Primitives.Long result = longCache.get(value);
		if (result == null) {
			result = new Primitives.Long(value);
			longCache.put(value, result);
		}
		return result;
	}
	
	private Map<Double, Primitives.Double> doubleCache = new HashMap<Double, Primitives.Double>(); 
	public Primitives.Double buildDouble(double value) {
		Primitives.Double result = doubleCache.get(value);
		if (result == null) {
			result = new Primitives.Double(value);
			doubleCache.put(value, result);
		}
		return result;
	}
	
	private Map<Float, Primitives.Float> floatCache = new HashMap<Float, Primitives.Float>(); 
	public Primitives.Float buildFloat(float value) {
		Primitives.Float result = floatCache.get(value);
		if (result == null) {
			result = new Primitives.Float(value);
			floatCache.put(value, result);
		}
		return result;
	}

	private Map<Byte, Primitives.Byte> byteCache = new HashMap<Byte, Primitives.Byte>(); 
	public Primitives.Byte buildByte(byte value) {
		Primitives.Byte result = byteCache.get(value);
		if (result == null) {
			result = new Primitives.Byte(value);
			byteCache.put(value, result);
		}
		return result;
	}

	private Map<Character, Primitives.Char> charCache = new HashMap<Character, Primitives.Char>(); 
	public Primitives.Char buildChar(char value) {
		Primitives.Char result = charCache.get(value);
		if (result == null) {
			result = new Primitives.Char(value);
			charCache.put(value, result);
		}
		return result;
	}

	private Primitives.Boolean TRUE = new Primitives.Boolean(true);
	private Primitives.Boolean FALSE = new Primitives.Boolean(false);
	public Primitives.Boolean buildBoolean(boolean value) {
		if (value)
			return TRUE;
		return FALSE;
	}

	private Primitives.Null NULL = new Primitives.Null();
	public Instance buildNull() {
		return NULL;
	}

	private static final Instance VOID = new Primitives.Void();
	public Instance buildVoid() {
		return VOID;
	}

}
