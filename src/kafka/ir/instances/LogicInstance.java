package kafka.ir.instances;

import kafka.ir.Builder;

public interface LogicInstance extends Instance {

	public LogicInstance leftShift(LogicInstance rhs, Builder builder);

	public LogicInstance rightShift(LogicInstance rhs, Builder builder);

	public LogicInstance reminder(LogicInstance rhs, Builder builder);

	public LogicInstance logicAnd(LogicInstance rhs, Builder builder);

	public LogicInstance logicOr(LogicInstance rhs, Builder builder);

	public LogicInstance logicXor(LogicInstance rhs, Builder builder);

}