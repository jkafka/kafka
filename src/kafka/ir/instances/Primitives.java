package kafka.ir.instances;

import java.util.HashMap;

import java.util.Map;

import kafka.ir.Builder;
import kafka.ir.types.PrimitiveTypes;
import kafka.ir.types.Type;

public class Primitives {
	
	public static class Null implements Instance {
		private static final Type TYPE = new PrimitiveTypes.NullType();

		@Override
		public Type type() {
			return TYPE;
		}
		
		@Override
		public String toString() {
			return "Null()";
		}
	}

	public static class Void implements Instance {
		private static final Type TYPE = new PrimitiveTypes.VoidType();

		@Override
		public Type type() {
			return TYPE;
		}
		
		@Override
		public String toString() {
			return "Void()";
		}
	}

	
	private static interface Integer {
		long value();
	}

	public static class Byte implements LogicInstance, NumericInstance, Integer, Instance {

		private final byte value;

		public Byte(byte value) {
			this.value = value;
		}

		@Override
		public Type type() {
			throw new AssertionError();
		}

		@Override
		public String toString() {
			return "Byte(" + value + ")";
		}

		@Override
		public long value() {
			return value;
		}
		
		@Override
		public NumericInstance plus(NumericInstance rhs, Builder builder) {
			return builder.buildInt(value + ((Byte)rhs).value);
		}

		@Override
		public NumericInstance minus(NumericInstance rhs, Builder builder) {
			return builder.buildInt(value - ((Byte)rhs).value);
		}

		@Override
		public NumericInstance times(NumericInstance rhs, Builder builder) {
			return builder.buildInt(value * ((Byte)rhs).value);
		}

		@Override
		public NumericInstance divide(NumericInstance rhs, Builder builder) {
			return builder.buildInt(value / ((Byte)rhs).value);
		}

		@Override
		public NumericInstance negate(Builder builder) {
			return builder.buildInt(-value);
		}
		
		@Override
		public LogicInstance leftShift(LogicInstance rhs, Builder builder) {
			return builder.buildInt(value << ((Integer)rhs).value());
		}

		@Override
		public LogicInstance rightShift(LogicInstance rhs, Builder builder) {
			return builder.buildInt(value >> ((Integer)rhs).value());
		}

		@Override
		public LogicInstance reminder(LogicInstance rhs, Builder builder) {
			return builder.buildInt(value % ((Byte)rhs).value);
		}

		@Override
		public LogicInstance logicAnd(LogicInstance rhs, Builder builder) {
			return builder.buildInt(value & ((Byte)rhs).value);
		}

		@Override
		public LogicInstance logicOr(LogicInstance rhs, Builder builder) {
			return builder.buildInt(value | ((Byte)rhs).value);
		}
		
		@Override
		public LogicInstance logicXor(LogicInstance rhs, Builder builder) {
			return builder.buildInt(value ^ ((Byte)rhs).value);
		}

		@Override
		public Instance toInt(Builder builder) {
			return builder.buildInt(value);
		}

		@Override
		public Instance toLong(Builder builder) {
			return builder.buildLong(value);
		}

		@Override
		public Instance toDouble(Builder builder) {
			return builder.buildDouble((double)value);
		}

		@Override
		public Instance toFloat(Builder builder) {
			return builder.buildFloat((float)value);
		}
		
		@Override
		public Instance toShort(Builder builder) {
			return builder.buildShort((short)value);
		}
		
		@Override
		public Instance toByte(Builder builder) {
			return this;
		}
		
		@Override
		public NumericInstance toChar(Builder builder) {
			return builder.buildChar((char)value);
		}
	}

	
	public static class Short implements LogicInstance, NumericInstance, Integer, Instance {

		private final int value;

		public Short(int value) {
			this.value = value;
		}

		@Override
		public Type type() {
			throw new AssertionError();
		}

		@Override
		public String toString() {
			return "Short(" + value + ")";
		}

		@Override
		public long value() {
			return value;
		}
		
		@Override
		public NumericInstance plus(NumericInstance rhs, Builder builder) {
			return builder.buildInt(value + ((Short)rhs).value);
		}

		@Override
		public NumericInstance minus(NumericInstance rhs, Builder builder) {
			return builder.buildInt(value - ((Short)rhs).value);
		}

		@Override
		public NumericInstance times(NumericInstance rhs, Builder builder) {
			return builder.buildInt(value * ((Short)rhs).value);
		}

		@Override
		public NumericInstance divide(NumericInstance rhs, Builder builder) {
			return builder.buildInt(value / ((Short)rhs).value);
		}
		
		@Override
		public NumericInstance negate(Builder builder) {
			return builder.buildInt(-value);
		}

		@Override
		public LogicInstance leftShift(LogicInstance rhs, Builder builder) {
			return builder.buildInt(value << ((Integer)rhs).value());
		}

		@Override
		public LogicInstance rightShift(LogicInstance rhs, Builder builder) {
			return builder.buildInt(value >> ((Integer)rhs).value());
		}

		@Override
		public LogicInstance reminder(LogicInstance rhs, Builder builder) {
			return builder.buildInt(value % ((Short)rhs).value);
		}

		@Override
		public LogicInstance logicAnd(LogicInstance rhs, Builder builder) {
			return builder.buildInt(value & ((Short)rhs).value);
		}

		@Override
		public LogicInstance logicOr(LogicInstance rhs, Builder builder) {
			return builder.buildInt(value | ((Short)rhs).value);
		}
		
		@Override
		public LogicInstance logicXor(LogicInstance rhs, Builder builder) {
			return builder.buildInt(value ^ ((Short)rhs).value);
		}

		@Override
		public Instance toInt(Builder builder) {
			return builder.buildInt(value);
		}

		@Override
		public Instance toLong(Builder builder) {
			return builder.buildLong(value);
		}

		@Override
		public Instance toDouble(Builder builder) {
			return builder.buildDouble((double)value);
		}

		@Override
		public Instance toFloat(Builder builder) {
			return builder.buildFloat((float)value);
		}
		
		@Override
		public Instance toShort(Builder builder) {
			return this;
		}
		
		@Override
		public Instance toByte(Builder builder) {
			return builder.buildByte((byte)value);
		}

		@Override
		public NumericInstance toChar(Builder builder) {
			return builder.buildChar((char)value);
		}
	}
	
	public static class Int implements NumericInstance, LogicInstance, Integer, Instance {

		private final int value;

		public Int(int value) {
			this.value = value;
		}

		@Override
		public Type type() {
			throw new AssertionError();
		}

		@Override
		public String toString() {
			return "Int(" + value + ")";
		}

		@Override
		public long value() {
			return value;
		}
		
		@Override
		public NumericInstance plus(NumericInstance rhs, Builder builder) {
			return builder.buildInt(value + ((Int)rhs).value);
		}

		@Override
		public NumericInstance minus(NumericInstance rhs, Builder builder) {
			return builder.buildInt(value - ((Int)rhs).value);
		}

		@Override
		public NumericInstance times(NumericInstance rhs, Builder builder) {
			return builder.buildInt(value * ((Int)rhs).value);
		}

		@Override
		public NumericInstance divide(NumericInstance rhs, Builder builder) {
			return builder.buildInt(value / ((Int)rhs).value);
		}
		
		@Override
		public NumericInstance negate(Builder builder) {
			return builder.buildInt(-value);
		}

		@Override
		public LogicInstance leftShift(LogicInstance rhs, Builder builder) {
			return builder.buildInt(value << ((Integer)rhs).value());
		}

		@Override
		public LogicInstance rightShift(LogicInstance rhs, Builder builder) {
			return builder.buildInt(value >> ((Integer)rhs).value());
		}

		@Override
		public LogicInstance reminder(LogicInstance rhs, Builder builder) {
			return builder.buildInt(value % ((Int)rhs).value);
		}

		@Override
		public LogicInstance logicAnd(LogicInstance rhs, Builder builder) {
			return builder.buildInt(value & ((Int)rhs).value);
		}

		@Override
		public LogicInstance logicOr(LogicInstance rhs, Builder builder) {
			return builder.buildInt(value | ((Int)rhs).value);
		}
		
		@Override
		public LogicInstance logicXor(LogicInstance rhs, Builder builder) {
			return builder.buildInt(value ^ ((Int)rhs).value);
		}

		public Instance toShort(Builder builder) {
			return builder.buildShort((short)value);
		}

		@Override
		public Instance toInt(Builder builder) {
			return this;
		}

		@Override
		public Instance toLong(Builder builder) {
			return builder.buildLong(value);
		}

		@Override
		public Instance toDouble(Builder builder) {
			return builder.buildDouble((double)value);
		}
		
		@Override
		public Instance toFloat(Builder builder) {
			return builder.buildFloat((float)value);
		}
		
		@Override
		public Instance toByte(Builder builder) {
			return builder.buildByte((byte)value);
		}
		
		@Override
		public NumericInstance toChar(Builder builder) {
			return builder.buildChar((char)value);
		}

		public boolean greaterThan(Int rhs) {
			return value > rhs.value;
		}

		public boolean equalTo(Int rhs) {
			return value == rhs.value;
		}

		public boolean lessThan(Int rhs) {
			return value < rhs.value;
		}		
	}

	public static class Long implements NumericInstance, LogicInstance, Integer, Instance {

		private final long value;

		public Long(long value) {
			this.value = value;
		}

		@Override
		public Type type() {
			throw new AssertionError();
		}

		@Override
		public String toString() {
			return "Long(" + value + ")";
		}

		@Override
		public long value() {
			return value;
		}
		
		@Override
		public NumericInstance plus(NumericInstance rhs, Builder builder) {
			return builder.buildLong(value + ((Long)rhs).value);
		}

		@Override
		public NumericInstance minus(NumericInstance rhs, Builder builder) {
			return builder.buildLong(value - ((Long)rhs).value);
		}

		@Override
		public NumericInstance times(NumericInstance rhs, Builder builder) {
			return builder.buildLong(value * ((Long)rhs).value);
		}

		@Override
		public NumericInstance divide(NumericInstance rhs, Builder builder) {
			return builder.buildLong(value / ((Long)rhs).value);
		}

		@Override
		public LogicInstance leftShift(LogicInstance rhs, Builder builder) {
			return builder.buildLong(value << ((Integer)rhs).value());
		}

		@Override
		public NumericInstance negate(Builder builder) {
			return builder.buildLong(-value);
		}

		@Override
		public LogicInstance rightShift(LogicInstance rhs, Builder builder) {
			return builder.buildLong(value >> ((Integer)rhs).value());
		}

		@Override
		public LogicInstance reminder(LogicInstance rhs, Builder builder) {
			return builder.buildLong(value % ((Long)rhs).value);
		}

		@Override
		public LogicInstance logicAnd(LogicInstance rhs, Builder builder) {
			return builder.buildLong(value & ((Long)rhs).value);
		}

		@Override
		public LogicInstance logicOr(LogicInstance rhs, Builder builder) {
			return builder.buildLong(value | ((Long)rhs).value);
		}
		
		@Override
		public LogicInstance logicXor(LogicInstance rhs, Builder builder) {
			return builder.buildLong(value ^ ((Long)rhs).value);
		}
		
		public Int toInt(Builder builder) {
			return builder.buildInt((int)value);
		}

		@Override
		public Instance toLong(Builder builder) {
			return this;
		}

		@Override
		public Instance toDouble(Builder builder) {
			return builder.buildDouble((double)value);
		}

		@Override
		public Instance toFloat(Builder builder) {
			return builder.buildFloat((float)value);
		}

		@Override
		public Instance toShort(Builder builder) {
			return builder.buildShort((short)value);
		}
		
		@Override
		public Instance toByte(Builder builder) {
			return builder.buildByte((byte)value);
		}
		
		@Override
		public NumericInstance toChar(Builder builder) {
			return builder.buildChar((char)value);
		}


		public boolean greaterThan(Long rhs) {
			return value > rhs.value;
		}

		public boolean equalTo(Long rhs) {
			return value == rhs.value;
		}

		public boolean lessThan(Long rhs) {
			return value < rhs.value;
		}		
	}

	
	public static class Double implements NumericInstance, Instance {

		private final double value;

		public Double(double value) {
			this.value = value;
		}

		@Override
		public Type type() {
			throw new AssertionError();
		}

		@Override
		public String toString() {
			return "Double(" + value + ")";
		}

		@Override
		public NumericInstance plus(NumericInstance rhs, Builder builder) {
			return builder.buildDouble(value + ((Double)rhs).value);
		}

		@Override
		public NumericInstance minus(NumericInstance rhs, Builder builder) {
			return builder.buildDouble(value - ((Double)rhs).value);
		}

		@Override
		public NumericInstance times(NumericInstance rhs, Builder builder) {
			return builder.buildDouble(value * ((Double)rhs).value);
		}

		@Override
		public NumericInstance divide(NumericInstance rhs, Builder builder) {
			return builder.buildDouble(value / ((Double)rhs).value);
		}

		@Override
		public NumericInstance negate(Builder builder) {
			return builder.buildDouble(-value);
		}
		
		@Override
		public Instance toInt(Builder builder) {
			return builder.buildInt((int)value);
		}

		@Override
		public Instance toLong(Builder builder) {
			return builder.buildLong((long)value);
		}

		@Override
		public Instance toDouble(Builder builder) {
			return this;
		}

		@Override
		public Instance toFloat(Builder builder) {
			return builder.buildFloat((float)value);
		}
		
		@Override
		public Instance toShort(Builder builder) {
			return builder.buildShort((short)value);
		}
		
		@Override
		public Instance toByte(Builder builder) {
			return builder.buildByte((byte)value);
		}
		
		@Override
		public NumericInstance toChar(Builder builder) {
			return builder.buildChar((char)value);
		}
		
		public boolean greaterThan(Double rhs) {
			return value > rhs.value;
		}

		public boolean equalTo(Double rhs) {
			return value == rhs.value;
		}

		public boolean lessThan(Double rhs) {
			return value < rhs.value;
		}

		public boolean isNan() {
			return java.lang.Double.isNaN(value);
		}		

	}

	public static class Float implements NumericInstance, Instance {

		private final float value;

		public Float(float value) {
			this.value = value;
		}

		@Override
		public Type type() {
			throw new AssertionError();
		}

		@Override
		public String toString() {
			return "Float(" + value + ")";
		}

		@Override
		public NumericInstance plus(NumericInstance rhs, Builder builder) {
			return builder.buildFloat(value + ((Float)rhs).value);
		}

		@Override
		public NumericInstance minus(NumericInstance rhs, Builder builder) {
			return builder.buildFloat(value - ((Float)rhs).value);
		}

		@Override
		public NumericInstance times(NumericInstance rhs, Builder builder) {
			return builder.buildFloat(value * ((Float)rhs).value);
		}

		@Override
		public NumericInstance divide(NumericInstance rhs, Builder builder) {
			return builder.buildFloat(value / ((Float)rhs).value);
		}

		@Override
		public NumericInstance negate(Builder builder) {
			return builder.buildFloat(-value);
		}
		
		@Override
		public Instance toInt(Builder builder) {
			return builder.buildInt((int)value);
		}		

		@Override
		public Instance toLong(Builder builder) {
			return builder.buildLong((long)value);
		}

		@Override
		public Instance toDouble(Builder builder) {
			return builder.buildDouble((double)value);
		}
		
		@Override
		public Instance toFloat(Builder builder) {
			return this;
		}
		
		@Override
		public Instance toShort(Builder builder) {
			return builder.buildShort((short)value);
		}
		
		@Override
		public Instance toByte(Builder builder) {
			return builder.buildByte((byte)value);
		}
		
		@Override
		public NumericInstance toChar(Builder builder) {
			return builder.buildChar((char)value);
		}

		
		public boolean greaterThan(Float rhs) {
			return value > rhs.value;
		}

		public boolean equalTo(Float rhs) {
			return value == rhs.value;
		}

		public boolean lessThan(Float rhs) {
			return value < rhs.value;
		}

		public boolean isNan() {
			return java.lang.Float.isNaN(value);
		}		

	}
	
	public static class Char implements NumericInstance {

		private final char value;

		public Char(char value) {
			this.value = value;
		}

		@Override
		public Type type() {
			throw new AssertionError();
		}

		@Override
		public String toString() {
			return "Char(" + value + ")";
		}

		@Override
		public Instance toInt(Builder builder) {
			return builder.buildInt((int)value);
		}

		@Override
		public Instance toLong(Builder builder) {
			return builder.buildDouble((long)value);
		}

		@Override
		public Instance toDouble(Builder builder) {
			return builder.buildDouble((double)value);
		}

		@Override
		public Instance toFloat(Builder builder) {
			return builder.buildFloat((float)value);
		}

		@Override
		public Instance toShort(Builder builder) {
			return builder.buildShort((short)value);
		}

		@Override
		public Instance toByte(Builder builder) {
			return builder.buildByte((byte)value);
		}

		@Override
		public NumericInstance plus(NumericInstance rhs, Builder builder) {
			return builder.buildInt(value + ((Char)rhs).value);
		}

		@Override
		public NumericInstance minus(NumericInstance rhs, Builder builder) {
			return builder.buildInt(value - ((Char)rhs).value);
		}

		@Override
		public NumericInstance times(NumericInstance rhs, Builder builder) {
			return builder.buildInt(value * ((Char)rhs).value);
		}

		@Override
		public NumericInstance divide(NumericInstance rhs, Builder builder) {
			return builder.buildInt(value - ((Char)rhs).value);
		}

		@Override
		public NumericInstance negate(Builder builder) {
			return builder.buildInt(-value);
		}
		
		@Override
		public NumericInstance toChar(Builder builder) {
			return this;
		}
	}

	
	public static class Boolean implements Instance {

		private final boolean value;

		public Boolean(boolean value) {
			this.value = value;
		}

		@Override
		public Type type() {
			throw new AssertionError();
		}

		@Override
		public String toString() {
			return "Boolean(" + value + ")";
		}
	}

	
	public static final class Array implements Instance {
		private final Int size;
		private final Type type;

		// TODO: Implement correct handling of store and load from array with
		// regards to known and unknown indexes.. 
		private final Map<java.lang.Integer, Instance> knowns = new HashMap<java.lang.Integer, Instance>();
		
		/**
		 * @param type type of the array (not the elements)
		 */
		public Array(Type type, Int size) {
			this.type = type;
			this.size = size;
		}
		
		@Override
		public Type type() {
			return type;
		}

		public Int size() {
			return size;
		}

		public void store(Int index, Instance value) {
//			assert value.type() == type;
			knowns.put((int)index.value(), value);			
		}

		public Instance load(Int index) {
			Instance value = knowns.get((int)index.value());
			assert value != null;
			return value;
		}

}

}
