package kafka.ir.instances;

import kafka.ir.types.Type;

public interface Instance {

	Type type();

}