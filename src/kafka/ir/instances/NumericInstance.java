package kafka.ir.instances;

import kafka.ir.Builder;

public interface NumericInstance extends Instance {

	public Instance toInt(Builder builder);

	public Instance toLong(Builder builder);

	public Instance toDouble(Builder builder);

	public Instance toFloat(Builder builder);

	public Instance toShort(Builder builder);

	public Instance toByte(Builder builder);

	public NumericInstance plus(NumericInstance rhs, Builder builder);
	
	public NumericInstance minus(NumericInstance rhs, Builder builder);

	public NumericInstance times(NumericInstance rhs, Builder builder);

	public NumericInstance divide(NumericInstance rhs, Builder builder);

	public NumericInstance negate(Builder builder);

	public NumericInstance toChar(Builder builder);

}