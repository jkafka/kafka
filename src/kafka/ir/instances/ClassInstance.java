package kafka.ir.instances;

import java.util.Map;

import kafka.ir.types.Field;
import kafka.ir.types.Method;
import kafka.ir.types.MethodReference;
import kafka.ir.types.ReferenceType;

public class ClassInstance implements Instance {
	private final ReferenceType type;
	private final Map<String, Field> fields;
	
	public ClassInstance(ReferenceType type, Map<String, Field> fields) {
		this.type = type;
		this.fields = fields;
	}
	
	@Override
	public ReferenceType type() {
		return type;
	}
	
	public Method method(MethodReference method) {
		return type.method(method);
	}

	@Override
	public String toString() {
		return "ClassInstance(" + type.name() + ")";
	}

	public Field field(String name) {
		return fields.get(name);
	}
	
}
