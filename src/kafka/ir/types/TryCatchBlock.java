package kafka.ir.types;

public class TryCatchBlock {

	private final Class exception;
	private final int startLbl;
	private final int endLbl;
	private final int handlerLbl;

	public TryCatchBlock(int startLbl, int endLbl, int handlerLbl, Class exception) {
		this.startLbl = startLbl;
		this.endLbl = endLbl;
		this.handlerLbl = handlerLbl;
		this.exception = exception;
	}

	@Override
	public String toString() {
		return "TryCatchBlock(start=" + startLbl + ", end=" + endLbl + ", handler=" +  handlerLbl + ", " + caughtException() + ")";
	}

	public boolean contains(int currentLbl) {
		return currentLbl >= startLbl & currentLbl < endLbl;
	}

	public int handlerLabel() {
		return handlerLbl;
	}

	public Class caughtException() {
		return exception;
	}
	

}
