package kafka.ir.types;

import java.util.Arrays;
import java.util.List;


import kafka.ir.instructions.Instruction;


public class Method {

	private final MethodReference reference;
	private final List<BasicBlock> bbs;
	private final int maxLocals;
	private final int maxStack;
	private final boolean isStatic;
	private final TryCatchBlock[] exceptionTable;
	private final boolean isNative;
	private final Class owner;

	public Method(List<BasicBlock> basicblock,
				  Class owner,
				  MethodReference methodReference,
				  boolean isStatic,
				  boolean isNative,
				  int maxStack,
				  int maxLocals,
				  TryCatchBlock[] exceptions) {
		this.bbs = basicblock;
		this.owner = owner;
		this.reference = methodReference;
		this.isStatic = isStatic;
		this.isNative = isNative;
		this.maxStack = maxStack;
		this.maxLocals = maxLocals;
		this.exceptionTable = exceptions;
	}
	
	
	public List<BasicBlock> basicblocks() {
		return bbs; 
	}
	
	@Override
	public String toString() {
		return reference().toString();
	}
	
	public String codeString() {
		StringBuilder b = new StringBuilder();
		for (Iterable<Instruction> bb : bbs)
			b.append(bb);
		
		if (exceptionTable != null) {
			b.append("exception table: " + Arrays.toString(exceptionTable) + "\n");
		}
		return b.toString();
	}

	public int maxLocals() {
		return maxLocals;
	}

	public int maxStack() {
		return maxStack;
	}

	public boolean isStatic() {
		return isStatic;
	}

	public boolean isNative() {
		return isNative;
	}
	
	public TryCatchBlock[] exceptionTable() {
		return exceptionTable;
	}

	public Class owner() {
		return owner;
	}

	public MethodReference reference() {
		return reference;
	}

}
