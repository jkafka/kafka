package kafka.ir.types;

import kafka.ir.Builder;
import kafka.ir.instances.ClassInstance;
import kafka.ir.instances.Instance;

public class RecursiveClass implements Class {
	SimpleClass impl;
	private final String name;
	
	public RecursiveClass(String name) {
		this.name = name;
	}

	public void init(SimpleClass impl) {
		assert impl != null;
		assert this.impl == null;
		this.impl = impl;
	}
	
	@Override
	public String toString() {
		assert impl != null : name;
		return impl.toString();
	}
	
	@Override
	public String name() {
		if (impl == null)
			return "Class(null)";
		return impl.name();
	}

	@Override
	public Method method(MethodReference ref) {
		assert impl != null : name;
		return impl.method(ref);
	}
	
	@Override
	public ClassInstance instantiate(Builder builder) {
		assert impl != null : name;
		return impl.instantiate(builder);
	}
	
	@Override
	public int hashCode() {
		// This is ok. Names of classes is the unique identifier.
		return name.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (obj instanceof Class) {
			// This is ok. Names of classes is the unique identifier.
			return name.equals(((Class)obj).name());
		}
		return false;
	}

	@Override
	public Field staticField(String name) {
		return impl.staticField(name);
	}

	@Override
	public Instance initializationValue(Builder builder) {
		return impl.initializationValue(builder);
	}

	@Override
	public boolean isSubclass(ReferenceType rhs) {
		return impl.isSubclass(rhs);
	}
	
}
