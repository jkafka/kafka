package kafka.ir.types;

import kafka.ir.Builder;
import kafka.ir.instances.ClassInstance;

public interface ReferenceType extends Type {
	Method method(MethodReference method);

	ClassInstance instantiate(Builder builder);

	boolean isSubclass(ReferenceType rhs);
	
}
