package kafka.ir.types;

import java.util.Map;

import kafka.ir.Builder;
import kafka.ir.instances.ClassInstance;
import kafka.ir.instances.Instance;
import kafka.utils.NoSuchKeyException;
import kafka.utils.NonNullMap;

public class SimpleClass implements Class {

	private final Map<MethodReference, Method> methods;
	private final String name;
	private final Map<String, Type> fields;
	private final Map<String, Field> staticFields;
	private final Class[] interfaces;
	private final Class superClass;

	public SimpleClass(String name,
				  Map<String, Type> fields,
				  Map<MethodReference, Method> methods,
				  Map<String, Field> staticFields,
				  Class superClass,
				  Class[] interfaces) {
		this.name = name;
		this.fields = fields;
		this.methods = methods;
		this.staticFields = staticFields;
		this.superClass = superClass;
		this.interfaces = interfaces;
		assert methods != null;
		assert interfaces != null;
	}

	public String codeString() {
		StringBuilder b = new StringBuilder();
		for (MethodReference ref: methods.keySet()) {
			b.append(ref + "\n");
			b.append(methods.get(ref).codeString());
			b.append("----------\n");
		}
		return b.toString();
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public String name() {
		return name;
	}
	
	@Override
	public Method method(MethodReference ref) {
		if (methods.containsKey(ref))
			return methods.get(ref);
		if (superClass != null)
			return superClass.method(ref);
		throw new NoSuchKeyException(ref);
	}
	
	@Override
	public ClassInstance instantiate(Builder builder) {
		Map<String, Field> fs = new NonNullMap<String, Field>();
		initFields(fs, builder);	
		return new ClassInstance(this, fs);
	}
	
	private void initFields(Map<String, Field> fs, Builder builder) {
		for (String name : fields.keySet()) {
			fs.put(name, new Field(fields.get(name).initializationValue(builder)));
		}
		
		if (superClass == null)
			return;
		
		SimpleClass actualSuperClass = null;
		if (superClass instanceof RecursiveClass)
			actualSuperClass = ((RecursiveClass)superClass).impl;
		else
			actualSuperClass = (SimpleClass)superClass;
		actualSuperClass.initFields(fs, builder);
	}

	@Override
	public int hashCode() {
		// This is ok. Names of classes is the unique identifier.
		return name.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (obj instanceof Class) {
			// This is ok. Names of classes is the unique identifier.
			return name.equals(((Class)obj).name());
		}
		return false;
	}

	@Override
	public Field staticField(String name) {
		return staticFields.get(name);
	}

	@Override
	public Instance initializationValue(Builder builder) {
		return builder.buildNull();
	}

	@Override
	public boolean isSubclass(ReferenceType rhs) {
		if (equals(rhs))
			return true;

		for (Class i : interfaces) {
			if (i.equals(rhs))
				return true;
		}
		
		if (superClass != null)
			return superClass.isSubclass(rhs);
			
		return false;
	}
}