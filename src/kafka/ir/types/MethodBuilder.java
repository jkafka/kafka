package kafka.ir.types;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.objectweb.asm.Label;

import kafka.ir.Builder;
import kafka.ir.instances.Primitives.Double;
import kafka.ir.instances.Primitives.Float;
import kafka.ir.instances.Primitives.Int;
import kafka.ir.instances.Primitives.Long;
import kafka.ir.instructions.Instruction;


public class MethodBuilder extends AbstractMethodBuilder {
	private static final class ExceptionTableEntry {

		final Label start;
		final Label end;
		final Label handler;
		final Class exceptionType;

		public ExceptionTableEntry(Label start, Label end, Label handler,
								   Class exceptionType) {
									this.start = start;
									this.end = end;
									this.handler = handler;
									this.exceptionType = exceptionType;
		}
		
	}
	private List<ExceptionTableEntry> exceptionTable = new ArrayList<ExceptionTableEntry>();	
	private List<ArrayList<Instruction>> basicblocks = new ArrayList<ArrayList<Instruction>>();
	
	// Label -> Basic block index.
	private Map<Label, Integer> labels = new HashMap<Label, Integer>();

	private ArrayList<Instruction> currentbb = new ArrayList<Instruction>();
	private Label currentLabel;
	
	private int maxStack = -1;
	private int maxLocals = -1;

	private final Map<MethodReference, Method> methods;

	private final Builder builder;

	private final MethodReference methodReference;

	private final boolean isStatic;
	private final boolean isNative;
	private final Class owner;
	
	public MethodBuilder(Class owner,
						 Map<MethodReference, Method> methods,
						 boolean isStatic,
					     boolean isNative,
					     MethodReference methodReference, 
					     Builder builder) {
				this.owner = owner;
				this.methods = methods;
				this.isStatic = isStatic;
				this.isNative = isNative;
				this.methodReference = methodReference;
				this.builder = builder;
	}

	@Override
	protected void addInstruction(Instruction i) {
		currentbb.add(i);
	}
	
	public void visitMaxs(int maxStack, int maxLocals) {
		this.maxStack = maxStack;
		this.maxLocals = maxLocals;
	}
	
	@Override
	public void visitLabel(Label label) {
		endBasicBlock(label);
		newBasicBlock(label);
	}

	private void newBasicBlock(Label label) {
		currentbb = new ArrayList<Instruction>();
		currentLabel = label;
		if (!labels.containsKey(label)) {
			labels.put(label, basicblocks.size());
			basicblocks.add(null);
		}
	}

	private void endBasicBlock(Label nextLabel) {
		if (currentbb.size() > 0) {
			int idx = indexForLabel(currentLabel);
			basicblocks.set(idx, currentbb);
		}
	}

	private int indexForLabel(Label label) {
		if (labels.get(label) == null)
			return buildLabel(label);
		return (int)labels.get(label);
	}
	
	@Override
	public void visitEnd() {
		endBasicBlock(null);
		List<BasicBlock> finishedBbs = finishHalfBuiltInstructions(basicblocks);
		
		TryCatchBlock[] exceptions = buildExceptionTable();
		methods.put(methodReference, new Method(finishedBbs,
											    owner,
												methodReference,
												isStatic,
												isNative,
												maxStack, maxLocals,
												exceptions));
	}

	private TryCatchBlock[] buildExceptionTable() {
		if (exceptionTable.isEmpty())
			return null;
		TryCatchBlock[] exceptions = new TryCatchBlock[exceptionTable.size()];
		for (int i = 0; i < exceptions.length; i++) {
			ExceptionTableEntry exc = exceptionTable.get(i);
			exceptions[i] = new TryCatchBlock(indexForLabel(exc.start),
											  indexForLabel(exc.end),
					                          indexForLabel(exc.handler),
					                          exc.exceptionType);
		}
		return exceptions;
	}
	
	private List<BasicBlock> finishHalfBuiltInstructions(List<ArrayList<Instruction>> basicblocks) {
		ArrayList<BasicBlock> finished = new ArrayList<BasicBlock>(basicblocks.size());
		for (int bbidx = 0; bbidx < basicblocks.size(); bbidx++) {
			ArrayList<Instruction> bb = basicblocks.get(bbidx);
			if (bb == null)
				continue;
			Instruction[] instructions = bb.toArray(new Instruction[bb.size()]);
			
			for (int i = 0; i < bb.size(); i++) {
				Instruction instr = instructions[i];
				if (instr instanceof AbstractMethodBuilder.HalfBuiltInstruction) {
					instructions[i] = ((HalfBuiltInstruction)instr).finish(this);
				}
			}
			
			finished.add(new BasicBlock(instructions, bbidx));
		}
		return finished;
	}

	@Override
	public void visitTryCatchBlock(Label start, Label end, Label handler, String type) {
		if (type == null) { // finally block
			exceptionTable.add(new ExceptionTableEntry(start, end, handler, null));
		} else {
			exceptionTable.add(new ExceptionTableEntry(start, end, handler,
				                                       builder.buildClass(type)));
		}
		
	}

	@Override
	protected Type buildType(String typeName) {
		return builder.buildType(typeName);
	}

	@Override
	protected MethodReference buildMethodReference(String methodName, String descriptor) {
		return builder.buildMethodReference(methodName, descriptor);
	}

	@Override
	protected Class buildClass(String className) {
		return builder.buildClass(className);
	}

	@Override
	protected Int buildPrimitive(int value) {
		return builder.buildInt(value);
	}

	@Override
	protected Long buildPrimitive(long value) {
		return builder.buildLong(value);
	}

	@Override
	protected Double buildPrimitive(double value) {
		return builder.buildDouble(value);
	}

	@Override
	protected Float buildPrimitive(float value) {
		return builder.buildFloat(value);
	}

	@Override
	protected int buildLabel(Label label) {
		Integer idx = labels.get(label);
		if (idx == null) {
			idx = basicblocks.size();
			labels.put(label, idx);
			basicblocks.add(null);
		}
		return idx.intValue();
	}

	@Override
	protected void addHalfBuiltInstruction(HalfBuiltInstruction i) {
		addInstruction(i);
	}

}
