package kafka.ir.types;

import kafka.ir.instances.Instance;


public class Field {

	private Instance value;

	public Field(Instance value) {
		this.value = value;
	}

	public Instance read() {
		return value;
	}

	public void write(Instance newValue) {
		assert newValue != null;
		value = newValue;
	}
	
	
}
