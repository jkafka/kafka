package kafka.ir.types;

import java.util.Map;

import kafka.ir.Builder;
import kafka.ir.instances.Instance;
import kafka.utils.NonNullMap;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.ClassNode;

public class ClassBuilder extends ClassVisitor {
	public Map<MethodReference, Method> methods = new NonNullMap<MethodReference, Method>();
	public Map<String, Type> fields = new NonNullMap<String, Type>();
	public Map<String, Field> staticFields = new NonNullMap<String, Field>();
	
	private String className;
	private Class[] interfaces;
	private final Builder builder;
	private Class superclass;

	public ClassBuilder(Builder builder) {
		super(Opcodes.ASM4, new ClassNode() { });
		this.builder = builder;
	}

	public void visit(int version, int access, String name, String signature,
			String superName, String[] interfaces) {
		this.className = name;
		this.interfaces = new Class[interfaces.length];
		for (int i = 0; i < interfaces.length; i++)
			this.interfaces[i] = builder.buildClass(interfaces[i]);

		if (superName != null)
			this.superclass = builder.buildClass("L" + superName + ";");
	}

	public MethodVisitor visitMethod(int access, String name, String desc,
			String signature, String[] exceptions) {
		boolean isStatic = isStatic(access);
		boolean isNative = isNative(access);
		return new MethodBuilder(builder.buildClass(className), 
								 methods, isStatic, isNative, builder.buildMethodReference(name, desc), builder);
	}


	@Override
	public FieldVisitor visitField(int access, String name, String desc,
			String signature, Object value) {
		if (isStatic(access)) {
			staticFields.put(name, new Field(buildStaticValue(value, desc)));
		} else {
			fields.put(name, builder.buildType(desc));
		}
		return null;
	}

	private static boolean isStatic(int access) {
		return (access & Opcodes.ACC_STATIC) != 0;
	}
	
	private static boolean isNative(int access) {
		return (access & Opcodes.ACC_NATIVE) != 0;
	}
	
	// TODO: ClassInstance and Primitives.* duplicates this functionality.
	private Instance buildStaticValue(Object value, String desc) {
		if (value == null)
			value = defaultStaticValue(desc);
		if (desc.equals("I"))
			return builder.buildInt((int)(java.lang.Integer)value);
		else if (desc.equals("J"))
			return builder.buildLong((long)(java.lang.Long)value);
		else if (desc.equals("Z"))
			return builder.buildBoolean(0 != (int)(java.lang.Integer)value);			
		else if (desc.equals("C"))
			return builder.buildChar((char)(int)(java.lang.Integer)value);
		else if (desc.equals("B"))
			return builder.buildByte((byte)(int)(java.lang.Integer)value);
		else if (desc.equals("S"))
			return builder.buildShort((short)(int)(java.lang.Integer)value);
		else if (desc.equals("D"))
			return builder.buildDouble((double)(java.lang.Double)value);			
		else if (desc.equals("F"))
			return builder.buildFloat((float)(java.lang.Float)value);
		else if (desc.equals("Ljava/lang/String;"))
			return null; // TODO: build string
		return builder.buildNull();
	}

	private static Object defaultStaticValue(String desc) {
		if (desc.length() == 1 & "ICBSZ".contains(desc)) // int, short, char, byte are all represented as Integer:s.
			return java.lang.Integer.valueOf(0);
		else if (desc.equals("J"))
			return java.lang.Long.valueOf(0);
//		else if (desc.equals("Z"))
//			return java.lang.Boolean.FALSE;			
		else if (desc.equals("D"))
			return java.lang.Double.valueOf(0);			
		else if (desc.equals("F"))
			return java.lang.Float.valueOf(0);
		else if (desc.equals("Ljava/lang/String;"))
			return null;
		return null;
	}

	public SimpleClass result() {
		return new SimpleClass(className, fields, methods, staticFields,
							   superclass, interfaces);
	}
	
}
