package kafka.ir.types;

import java.util.HashMap;

import kafka.ir.Builder;
import kafka.ir.instances.ClassInstance;
import kafka.ir.instances.Instance;
import kafka.utils.NonNullMap;

public class PrimitiveTypes {
	public static class NullType implements ReferenceType {

		@Override
		public String name() {
			return "NullType";
		}

		@Override
		public Instance initializationValue(Builder builder) {
			throw new AssertionError();
		}

		@Override
		public Method method(MethodReference method) {
			throw new AssertionError();
		}

		@Override
		public ClassInstance instantiate(Builder builder) {
			throw new AssertionError();
		}

		@Override
		public boolean isSubclass(ReferenceType rhs) {
			return true;
		}

	}

	private static final HashMap<String, Field> EMPTY_FIELDS_MAP = new NonNullMap<String, Field>();

	public abstract static class Primitive implements Type {
		private final String name;

		public Primitive(String name) {
			this.name = name;
		}

		@Override
		public String name() {
			return name;
		}
		
		@Override
		public String toString() {
			return this.getClass().getSimpleName() + "(" + name + ")";
		}
		
	}
	
	public static class Short extends Primitive {
		public Short() {
			super("short");
		}

		@Override
		public Instance initializationValue(Builder builder) {
			return builder.buildShort((short)0);
		}
	}

	public static class Double extends Primitive {
		public Double() {
			super("double");
		}
		
		@Override
		public Instance initializationValue(Builder builder) {
			return builder.buildDouble(0);
		}
	}

	public static class Float extends Primitive {
		public Float() {
			super("float");
		}
		
		@Override
		public Instance initializationValue(Builder builder) {
			return builder.buildFloat(0);
		}
	}

	public static class Boolean extends Primitive {
		public Boolean() {
			super("boolean");
		}
		
		@Override
		public Instance initializationValue(Builder builder) {
			return builder.buildBoolean(false);
		}
	}

	public static class Byte extends Primitive {
		public Byte() {
			super("byte");
		}
		
		@Override
		public Instance initializationValue(Builder builder) {
			return builder.buildByte((byte)0);
		}
	}

	public static class Long extends Primitive {
		public Long() {
			super("long");
		}
		
		@Override
		public Instance initializationValue(Builder builder) {
			return builder.buildLong(0);
		}
	}
	
	public static class Int extends Primitive {
		public Int() {
			super("int");
		}
		
		@Override
		public Instance initializationValue(Builder builder) {
			return builder.buildInt(0);
		}
	}
	
	public static class Char extends Primitive {
		public Char() {
			super("char");
		}
		
		@Override
		public Instance initializationValue(Builder builder) {
			return builder.buildChar((char)0);
		}
	}
	
	public static class VoidType extends Primitive {
		public VoidType() {
			super("void");
		}
		
		@Override
		public Instance initializationValue(Builder builder) {
			throw new AssertionError();
		}
	}

	public static class Array implements ReferenceType {

		private final Type elementType;

		public Array(Type elementType) {
			this.elementType = elementType;
		}

		@Override
		public String name() {
			return elementType.name() + "[]";
		}
		
		public Type elementType() {
			return elementType;
		}

		@Override
		public ClassInstance instantiate(Builder builder) {
			return new ClassInstance(this, EMPTY_FIELDS_MAP);
		}
		
		@Override
		public Method method(MethodReference ref) {
			throw new AssertionError("Primitived does not have function");
		}
		
		@Override
		public Instance initializationValue(Builder builder) {
			return builder.buildNull();
		}

		@Override
		public boolean isSubclass(ReferenceType rhs) {
			throw new AssertionError("TODO: Is this possible for arrays?");
		}
	}



}
