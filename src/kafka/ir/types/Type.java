package kafka.ir.types;

import kafka.ir.Builder;
import kafka.ir.instances.Instance;

public interface Type {
	String name();	
	Instance initializationValue(Builder builder);
	
}
