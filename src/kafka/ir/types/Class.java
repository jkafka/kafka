package kafka.ir.types;


public interface Class extends ReferenceType {

	Method method(MethodReference method);
	Field staticField(String name);
}