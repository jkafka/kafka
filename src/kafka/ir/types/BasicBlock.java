package kafka.ir.types;

import java.util.Iterator;

import kafka.ir.instructions.Instruction;


public class BasicBlock implements Iterable<Instruction> {
	private Instruction[] instructions;
	private final int label;
	
	
	public BasicBlock(Instruction[] instructions, int currentLabel) {
		this.label = currentLabel;
		this.instructions = instructions;
	}
	
	@Override
	public String toString() {
		StringBuilder b = new StringBuilder();
		b.append("  label" + label + ":\n");
		for (Instruction i : instructions)
			b.append("    " + i + "\n");
		return b.toString();
	}

	@Override
	public Iterator<Instruction> iterator() {
		return new Iterator<Instruction>() {
			int count = 0;
			@Override
			public boolean hasNext() {
				return count < instructions.length;
			}

			@Override
			public Instruction next() {
				return instructions[count++];
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			} };
	}
}
