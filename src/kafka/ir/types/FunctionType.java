package kafka.ir.types;

import java.util.Arrays;

public class FunctionType {

	private final Type returnType;
	private final Type[] argTypes;

	public FunctionType(Type returnType, Type[] argTypes) {
		this.returnType = returnType;
		this.argTypes = argTypes;
	}
	
	public String name() {
		StringBuilder b = new StringBuilder();
		b.append(returnType.name());
		b.append("(");
		for (int i = 0; i < argTypes.length; i++) {
			b.append(argTypes[i].name());
			if (i < argTypes.length - 1)
				b.append(", ");
		}
		b.append(")");
		return b.toString();
	}
	
	@Override
	public String toString() {
		return name();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(argTypes);
		result = prime * result
				+ ((returnType == null) ? 0 : returnType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FunctionType other = (FunctionType) obj;
		if (!Arrays.equals(argTypes, other.argTypes))
			return false;
		if (returnType == null) {
			if (other.returnType != null)
				return false;
		} else if (!returnType.equals(other.returnType))
			return false;
		return true;
	}

	public final Type[] arguments() {
		return argTypes;
	}
	

}
