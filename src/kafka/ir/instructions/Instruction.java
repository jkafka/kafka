package kafka.ir.instructions;


public interface Instruction {

	void visit(InstructionVisitor visitor);
}
