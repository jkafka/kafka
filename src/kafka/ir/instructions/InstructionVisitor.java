package kafka.ir.instructions;

import kafka.ir.types.AbstractMethodBuilder;

public interface InstructionVisitor {
	void visit(AbstractMethodBuilder.Nop instruction);

	void visit(AbstractMethodBuilder.AConstNull instruction);

	void visit(AbstractMethodBuilder.IConst instruction);

	void visit(AbstractMethodBuilder.LConst instruction);

	void visit(AbstractMethodBuilder.FConst instruction);

	void visit(AbstractMethodBuilder.DConst instruction);

	void visit(AbstractMethodBuilder.Iaload instruction);

	void visit(AbstractMethodBuilder.Laload instruction);

	void visit(AbstractMethodBuilder.Faload instruction);

	void visit(AbstractMethodBuilder.Daload instruction);

	void visit(AbstractMethodBuilder.Aaload instruction);

	void visit(AbstractMethodBuilder.Baload instruction);

	void visit(AbstractMethodBuilder.Caload instruction);

	void visit(AbstractMethodBuilder.Saload instruction);

	void visit(AbstractMethodBuilder.Iastore instruction);

	void visit(AbstractMethodBuilder.Lastore instruction);

	void visit(AbstractMethodBuilder.Fastore instruction);

	void visit(AbstractMethodBuilder.Dastore instruction);

	void visit(AbstractMethodBuilder.Aastore instruction);

	void visit(AbstractMethodBuilder.Bastore instruction);

	void visit(AbstractMethodBuilder.Castore instruction);

	void visit(AbstractMethodBuilder.Sastore instruction);

	void visit(AbstractMethodBuilder.Pop instruction);

	void visit(AbstractMethodBuilder.Pop2 instruction);

	void visit(AbstractMethodBuilder.Dup instruction);

	void visit(AbstractMethodBuilder.Dup_x1 instruction);

	void visit(AbstractMethodBuilder.Dup_x2 instruction);

	void visit(AbstractMethodBuilder.Dup2 instruction);

	void visit(AbstractMethodBuilder.Dup2_x1 instruction);

	void visit(AbstractMethodBuilder.Dup2_x2 instruction);

	void visit(AbstractMethodBuilder.Swap instruction);

	void visit(AbstractMethodBuilder.Add instruction);

	void visit(AbstractMethodBuilder.Sub instruction);

	void visit(AbstractMethodBuilder.Mul instruction);

	void visit(AbstractMethodBuilder.Div instruction);

	void visit(AbstractMethodBuilder.Rem instruction);

	void visit(AbstractMethodBuilder.Ineg instruction);

	void visit(AbstractMethodBuilder.Lneg instruction);

	void visit(AbstractMethodBuilder.Fneg instruction);

	void visit(AbstractMethodBuilder.Dneg instruction);

	void visit(AbstractMethodBuilder.Shl instruction);

	void visit(AbstractMethodBuilder.Shr instruction);

	void visit(AbstractMethodBuilder.Ushr instruction);

	void visit(AbstractMethodBuilder.And instruction);

	void visit(AbstractMethodBuilder.Or instruction);

	void visit(AbstractMethodBuilder.Xor instruction);

	void visit(AbstractMethodBuilder.I2l instruction);

	void visit(AbstractMethodBuilder.I2f instruction);

	void visit(AbstractMethodBuilder.I2d instruction);

	void visit(AbstractMethodBuilder.L2i instruction);

	void visit(AbstractMethodBuilder.L2f instruction);

	void visit(AbstractMethodBuilder.L2d instruction);

	void visit(AbstractMethodBuilder.F2i instruction);

	void visit(AbstractMethodBuilder.F2l instruction);

	void visit(AbstractMethodBuilder.F2d instruction);

	void visit(AbstractMethodBuilder.D2i instruction);

	void visit(AbstractMethodBuilder.D2l instruction);

	void visit(AbstractMethodBuilder.D2f instruction);

	void visit(AbstractMethodBuilder.I2b instruction);

	void visit(AbstractMethodBuilder.I2c instruction);

	void visit(AbstractMethodBuilder.I2s instruction);

	void visit(AbstractMethodBuilder.Lcmp instruction);
	
	void visit(AbstractMethodBuilder.Icmp instruction);
	
	void visit(AbstractMethodBuilder.Acmp instruction);

	void visit(AbstractMethodBuilder.Fcmpl instruction);

	void visit(AbstractMethodBuilder.Fcmpg instruction);

	void visit(AbstractMethodBuilder.Dcmpl instruction);

	void visit(AbstractMethodBuilder.Dcmpg instruction);

	void visit(AbstractMethodBuilder.Ireturn instruction);

	void visit(AbstractMethodBuilder.Lreturn instruction);

	void visit(AbstractMethodBuilder.Freturn instruction);

	void visit(AbstractMethodBuilder.Dreturn instruction);

	void visit(AbstractMethodBuilder.Areturn instruction);

	void visit(AbstractMethodBuilder.Returnv instruction);

	void visit(AbstractMethodBuilder.Arraylength instruction);

	void visit(AbstractMethodBuilder.Athrow instruction);

	void visit(AbstractMethodBuilder.Monitorenter instruction);

	void visit(AbstractMethodBuilder.Monitorexit instruction);

	void visit(AbstractMethodBuilder.Strconst instruction);
	
	void visit(AbstractMethodBuilder.Typeconst instruction);

	void visit(AbstractMethodBuilder.Bipush instruction);

	void visit(AbstractMethodBuilder.Sipush instruction);

	void visit(AbstractMethodBuilder.Newarray instruction);

	void visit(AbstractMethodBuilder.Iload instruction);

	void visit(AbstractMethodBuilder.Lload instruction);

	void visit(AbstractMethodBuilder.Fload instruction);

	void visit(AbstractMethodBuilder.Dload instruction);

	void visit(AbstractMethodBuilder.Aload instruction);

	void visit(AbstractMethodBuilder.Istore instruction);

	void visit(AbstractMethodBuilder.Lstore instruction);

	void visit(AbstractMethodBuilder.Fstore instruction);

	void visit(AbstractMethodBuilder.Dstore instruction);

	void visit(AbstractMethodBuilder.Astore instruction);

	void visit(AbstractMethodBuilder.Ret instruction);

	void visit(AbstractMethodBuilder.New instruction);

	void visit(AbstractMethodBuilder.Anewarray instruction);

	void visit(AbstractMethodBuilder.Checkcast instruction);

	void visit(AbstractMethodBuilder.Instanceof instruction);

	void visit(AbstractMethodBuilder.Getstatic instruction);

	void visit(AbstractMethodBuilder.Putstatic instruction);

	void visit(AbstractMethodBuilder.Getfield instruction);

	void visit(AbstractMethodBuilder.Putfield instruction);

	void visit(AbstractMethodBuilder.Invokevirtual instruction);

	void visit(AbstractMethodBuilder.Invokespecial instruction);

	void visit(AbstractMethodBuilder.Invokestatic instruction);

	void visit(AbstractMethodBuilder.Invokeinterface instruction);

	void visit(AbstractMethodBuilder.Ifeq instruction);

	void visit(AbstractMethodBuilder.Ifne instruction);

	void visit(AbstractMethodBuilder.Iflt instruction);

	void visit(AbstractMethodBuilder.Ifge instruction);

	void visit(AbstractMethodBuilder.Ifgt instruction);

	void visit(AbstractMethodBuilder.Ifle instruction);

	void visit(AbstractMethodBuilder.Goto instruction);

	void visit(AbstractMethodBuilder.Jsr instruction);

	void visit(AbstractMethodBuilder.Ifnull instruction);

	void visit(AbstractMethodBuilder.Ifnonnull instruction);

	void visit(AbstractMethodBuilder.Ldc instruction);

	void visit(AbstractMethodBuilder.Iinc instruction);

	void visit(AbstractMethodBuilder.Tableswitch instruction);

	void visit(AbstractMethodBuilder.Lookupswitch instruction);

	void visit(AbstractMethodBuilder.Multianewarray instruction);
}