package kafka.natives;

import kafka.ir.instances.Instance;

public class NativeThrowable {
	public Instance fillInStackTrace(Instance[] args) {
		return args[0];
	}
}
