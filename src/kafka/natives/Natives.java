package kafka.natives;

import java.util.HashMap;
import java.util.Map;

import kafka.ir.Builder;
import kafka.ir.types.Class;

public class Natives {
	public static Map<Class, Object> init(Builder builder) {
		HashMap<Class, Object> natives = new HashMap<Class, Object>();
		
		add(natives, builder, java.lang.Throwable.class, new NativeThrowable());
		add(natives, builder, java.lang.Object.class, new NativeObject(builder));
		add(natives, builder, java.lang.Class.class, new NativeClass(builder));
		
		return natives;
	}

	private static void add(HashMap<Class, Object> natives,
							Builder builder,
							java.lang.Class<?> cls,
							Object nativeImpl) {
		String className = "L" + cls.getName().replace('.', '/') + ";";
		natives.put(builder.buildClass(className),
					nativeImpl);
	}
}
