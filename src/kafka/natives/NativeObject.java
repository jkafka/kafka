package kafka.natives;

import kafka.ir.Builder;
import kafka.ir.instances.Instance;

public class NativeObject {
	private final Builder builder;

	public NativeObject(Builder builder) {
		this.builder = builder;
	}

	public Instance registerNatives(Instance[] args) {
		return builder.buildVoid();
	}
}
