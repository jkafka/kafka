package kafka.natives;

import kafka.ir.Builder;
import kafka.ir.instances.Instance;

public class NativeClass {
	private final Builder builder;

	public NativeClass(Builder builder) {
		this.builder = builder;
	}

	public Instance getPrimitiveClass(Instance[] args) {
		return builder.buildNull();
	}

}
