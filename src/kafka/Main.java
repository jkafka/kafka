package kafka;

import kafka.ir.Builder;
import kafka.ir.instances.ClassInstance;
import kafka.ir.instances.Instance;
import kafka.ir.types.Class;
import kafka.ir.types.MethodReference;
import kafka.ir.types.ReferenceType;
import kafka.vm.Vm;

public class Main {
	public static void main(final String[] args) throws Exception {
		Builder builder = new Builder();
		Class built = builder.buildClass("L" + args[0].replace(".", "/") + ";");
		MethodReference main = builder.buildMethodReference("main", "([Ljava/lang/String;)V");
		Vm vm = new Vm(null, builder);
		ReferenceType type = (ReferenceType) builder.buildType("[Ljava/lang/String;");
		ClassInstance[] params = new ClassInstance[] { type.instantiate(builder) };
		Instance retvalues = vm.invoke(built.method(main), params);
		System.out.println(retvalues);
	}

}
